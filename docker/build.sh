#!/usr/bin/env bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
mvn clean package -Dmaven.test.skip=true -Dmaven.javadoc.skip=true -Dsource.skip=true -B -T4C
docker build -t "ictgroup.it/btest1" -f docker/Dockerfile .

cd docker
docker-compose up


