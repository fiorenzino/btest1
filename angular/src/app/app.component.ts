import { Component } from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Rendicontazione';

  items: MenuItem[];

  ngOnInit() {
    this.items = [
        {label: 'Attivita', icon: 'fa fa-fw fa-bar-chart', routerLink: "/attivitas"},
        {label: 'Autorizzazioni', icon: 'fa fa-fw fa-calendar', routerLink:"/autorizzazioni"},
        {label: 'Clienti', icon: 'fa fa-fw fa-book', routerLink:"/clienti"},
        {label: 'Progetti', icon: 'fa fa-fw fa-support', routerLink:"/progetti"},
        {label: 'Sviluppatori', icon: 'fa fa-fw fa-twitter',routerLink:"/sviluppatori"}
    ];
}
}
