import { Progetto } from '../model/progetto';
import { Cliente } from '../model/cliente';
import { Sviluppatore } from '../model/sviluppatore';

export const PROGETTI: Progetto[] = [
    {uuid: '1',nome: 'prova',codice: 'P01',cliente: new Cliente, uuid_cliente: '',responsabile: '',sviluppatore: new Sviluppatore, active: true}
];