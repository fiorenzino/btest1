import { Autorizzazione} from '../model/autorizzazione';
import { Progetto } from '../model/progetto';
import { Sviluppatore } from '../model/sviluppatore';

export const AUTORIZZAZIONI : Autorizzazione[] = [
    {uuid: '1', data_inizio: new Date , data_fine: new Date, uuid_progetto: '',progetto: new Progetto, uuid_sviluppatore: '',sviluppatore: new Sviluppatore,  active: true}
];