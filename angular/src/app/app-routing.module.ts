import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AttivitasComponent } from './components/attivitas/attivitas.component';
import { AttivitasExportComponent } from './components/attivitas/attivitas-export.component';
import { AttivitasUploadComponent } from './components/attivitas/attivitas-upload.component';
import { AttivitaSearchComponent } from './components/attivitas/attivita-search.component';
import { AttivitaAddComponent } from './components/attivitas/attivita-add.component';
import { AttivitaDetailComponent } from './components/attivitas/attivita-detail.component';
import { AttivitaEditComponent } from './components/attivitas/attivita-edit.component';

import { AutorizzazioniComponent } from './components/autorizzazioni/autorizzazioni.component';
import { AutorizzazioneSearchComponent } from './components/autorizzazioni/autorizzazione-search.component';
import { AutorizzazioneAddComponent } from './components/autorizzazioni/autorizzazione-add.component';
import { AutorizzazioneEditComponent } from './components/autorizzazioni/autorizzazione-edit.component';
import { AutorizzazioneDetailComponent } from './components/autorizzazioni/autorizzazione-detail.component';

import { ClientiComponent } from './components/clienti/clienti.component';
import { ClienteSearchComponent } from './components/clienti/cliente-search.component';
import { ClienteAddComponent } from './components/clienti/cliente-add.component';
import { ClienteEditComponent } from './components/clienti/cliente-edit.component';
import { ClienteDetailComponent } from './components/clienti/cliente-detail.component';

import { ProgettiComponent } from './components/progetti/progetti.component';
import { ProgettoSearchComponent } from './components/progetti/progetto-search.component';
import { ProgettoAddComponent } from './components/progetti/progetto-add.component';
import { ProgettoEditComponent } from './components/progetti/progetto-edit.component';
import { ProgettoDetailComponent } from './components/progetti/progetto-detail.component';

import { SviluppatoriComponent } from './components/sviluppatori/sviluppatori.component';
import { SviluppatoreSearchComponent } from './components/sviluppatori/sviluppatore-search.component';
import { SviluppatoreAddComponent } from './components/sviluppatori/sviluppatore-add.component';
import { SviluppatoreEditComponent } from './components/sviluppatori/sviluppatore-edit.component';
import { SviluppatoreDetailComponent } from './components/sviluppatori/sviluppatore-detail.component';

const routes: Routes = [
  { path: 'attivitas', component: AttivitasComponent},
  { path: 'attivita-search', component: AttivitaSearchComponent},
  { path: 'attivita-upload', component: AttivitasUploadComponent},
  { path: 'attivita-export', component: AttivitasExportComponent},
  { path: 'attivita-add', component: AttivitaAddComponent},
  { path: 'attivita-detail/:uuid', component: AttivitaDetailComponent},
  { path: 'attivita-edit/:uuid', component: AttivitaEditComponent},

  { path: 'autorizzazioni', component: AutorizzazioniComponent},
  { path: 'autorizzazione-search', component: AutorizzazioneSearchComponent},
  { path: 'autorizzazione-add', component: AutorizzazioneAddComponent},
  { path: 'autorizzazione-edit/:uuid', component: AutorizzazioneEditComponent},
  { path: 'autorizzazione-detail/:uuid', component: AutorizzazioneDetailComponent},

  { path: 'clienti', component: ClientiComponent},
  { path: 'cliente-search', component: ClienteSearchComponent},
  { path: 'cliente-add', component: ClienteAddComponent},
  { path: 'cliente-edit/:uuid', component: ClienteEditComponent},
  { path: 'cliente-detail/:uuid', component: ClienteDetailComponent},

  { path: 'progetti', component: ProgettiComponent},
  { path: 'progetto-search', component: ProgettoSearchComponent},
  { path: 'progetto-add', component: ProgettoAddComponent},
  { path: 'progetto-edit/:uuid', component: ProgettoEditComponent},
  { path: 'progetto-detail/:uuid', component: ProgettoDetailComponent},

  { path: 'sviluppatori', component: SviluppatoriComponent},
  { path: 'sviluppatore-search', component: SviluppatoreSearchComponent},
  { path: 'sviluppatore-add', component: SviluppatoreAddComponent},
  { path: 'sviluppatore-edit/:uuid', component: SviluppatoreEditComponent},
  { path: 'sviluppatore-detail/:uuid', component: SviluppatoreDetailComponent},
  
]
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
