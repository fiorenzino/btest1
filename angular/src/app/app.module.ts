import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './services/in-memory-data.service';
import { HttpClientModule } from '@angular/common/http';
import { FileUploadModule, FileUpload } from 'primeng/primeng';
import {CardModule} from 'primeng/card';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {TabMenuModule} from 'primeng/tabmenu';
import {GrowlModule} from 'primeng/growl';


import { AttivitasComponent } from './components/attivitas/attivitas.component';
import { AttivitaSearchComponent } from './components/attivitas/attivita-search.component';
import { AttivitasUploadComponent } from './components/attivitas/attivitas-upload.component';
import { AttivitasExportComponent } from './components/attivitas/attivitas-export.component';
import { AttivitaAddComponent } from './components/attivitas/attivita-add.component';
import { AttivitaDetailComponent } from './components/attivitas/attivita-detail.component';
import { AttivitaEditComponent } from './components/attivitas/attivita-edit.component';

import { AutorizzazioniComponent } from './components/autorizzazioni/autorizzazioni.component';
import { AutorizzazioneSearchComponent } from './components/autorizzazioni/autorizzazione-search.component';
import { AutorizzazioneDetailComponent } from './components/autorizzazioni/autorizzazione-detail.component';

import { ClientiComponent } from './components/clienti/clienti.component';
import { ClienteSearchComponent } from './components/clienti/cliente-search.component';
import { ClienteDetailComponent } from './components/clienti/cliente-detail.component';

import { ProgettiComponent } from './components/progetti/progetti.component';
import { ProgettoSearchComponent } from './components/progetti/progetto-search.component';
import { ProgettoDetailComponent } from './components/progetti/progetto-detail.component';

import { SviluppatoriComponent } from './components/sviluppatori/sviluppatori.component';
import { SviluppatoreSearchComponent } from './components/sviluppatori/sviluppatore-search.component';
import { SviluppatoreDetailComponent } from './components/sviluppatori/sviluppatore-detail.component';
import { AutorizzazioneEditComponent } from './components/autorizzazioni/autorizzazione-edit.component';
import { ProgettoEditComponent } from './components/progetti/progetto-edit.component';
import { ProgettoAddComponent } from './components/progetti/progetto-add.component';
import { AutorizzazioneAddComponent } from './components/autorizzazioni/autorizzazione-add.component';
import { ClienteAddComponent } from './components/clienti/cliente-add.component';
import { ClienteEditComponent } from './components/clienti/cliente-edit.component';
import { SviluppatoreEditComponent } from './components/sviluppatori/sviluppatore-edit.component';
import { SviluppatoreAddComponent } from './components/sviluppatori/sviluppatore-add.component';


@NgModule({
  declarations: [
    AppComponent,
    ClientiComponent,
    ClienteDetailComponent,
    AttivitasComponent,
    AutorizzazioniComponent,
    AutorizzazioneDetailComponent,
    ProgettiComponent,
    ProgettoDetailComponent,
    SviluppatoriComponent,
    SviluppatoreDetailComponent,
    AttivitaDetailComponent,
    ClienteSearchComponent,
    AttivitaSearchComponent,
    AutorizzazioneSearchComponent,
    ProgettoSearchComponent,
    SviluppatoreSearchComponent,
    AttivitasUploadComponent,
    AttivitasExportComponent,
    AttivitaAddComponent,
    AttivitaEditComponent,
    AutorizzazioneEditComponent,
    ProgettoEditComponent,
    ProgettoAddComponent,
    AutorizzazioneAddComponent,
    ClienteAddComponent,
    ClienteEditComponent,
    SviluppatoreEditComponent,
    SviluppatoreAddComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    FileUploadModule,
    CardModule,
    ConfirmDialogModule,
    TabMenuModule,
    GrowlModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
