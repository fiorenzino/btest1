import { TestBed, inject } from '@angular/core/testing';

import { SviluppatoreService } from './sviluppatore.service';

describe('SviluppatoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SviluppatoreService]
    });
  });

  it('should be created', inject([SviluppatoreService], (service: SviluppatoreService) => {
    expect(service).toBeTruthy();
  }));
});
