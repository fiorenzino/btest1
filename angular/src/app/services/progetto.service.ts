import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {PROGETTI} from '../mock/mock-progetti';
import { Progetto} from '../model/progetto';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ProgettoService {
  private progettiUrl = '/api/v1/progetti';  // URL to web api

  getProgetti(params:Object):Observable<Progetto[]>{
    return this.http.get<Progetto[]>(this.progettiUrl, params)
    .pipe(
      catchError(this.handleError('getProgetti', [])))
  }

  getProgetto(uuid: string): Observable<Progetto> {
    const url = `${this.progettiUrl}/${uuid}`;
    return this.http.get<Progetto>(url).pipe(
      catchError(this.handleError<Progetto>(`getProgetto uuid=${uuid}`))
    );
  }

  constructor(
    private http: HttpClient
  ) { }


  /** PUT: update the progetto on the server */
  updateProgetto (progetto: Progetto): Observable<any> {
    return this.http.put(this.progettiUrl, progetto, httpOptions).pipe(
      catchError(this.handleError<any>('updateProgetto'))
    );
  }

  /** POST: add a new progetto to the server */
  addProgetto (progetto: Progetto): Observable<Progetto> {
    return this.http.post<Progetto>(this.progettiUrl, progetto, httpOptions).pipe(
      catchError(this.handleError<Progetto>('addProgetto'))
    );
  }

  /** DELETE: delete the progetto from the server */
  deleteProgetto (progetto: Progetto | number): Observable<Progetto> {
    const uuid = typeof progetto === 'number' ? progetto : progetto.uuid;
    const url = `${this.progettiUrl}/${uuid}`;

    return this.http.delete<Progetto>(url, httpOptions).pipe(
      catchError(this.handleError<Progetto>('deleteProgetto'))
    );
  }

  /* GET progetti whose name contains search term */
  searchProgetti(term: string): Observable<Progetto[]> {
    if (!term.trim()) {
      // if not search term, return empty progetto array.
      return of([]);
    }
    return this.http.get<Progetto[]>(`${this.progettiUrl}/search?codice=${term}`).pipe(
      catchError(this.handleError<Progetto[]>('searchProgetti', []))
    );
  }



   /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
