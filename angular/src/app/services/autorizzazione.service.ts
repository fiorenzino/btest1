import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {AUTORIZZAZIONI} from '../mock/mock-autorizzazioni';
import { Autorizzazione} from '../model/autorizzazione';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AutorizzazioneService {
  private autorizzazioniUrl = '/api/v1/autorizzazioni';  // URL to web api

  getAutorizzazioni(params:Object):Observable<Autorizzazione[]>{
    return this.http.get<Autorizzazione[]>(this.autorizzazioniUrl,params)
    .pipe(
      catchError(this.handleError('getAutorizzazioni', [])))
  }
  
  getAutorizzazione(uuid: string): Observable<Autorizzazione> {
    const url = `${this.autorizzazioniUrl}/${uuid}`;
    return this.http.get<Autorizzazione>(url).pipe(
      catchError(this.handleError<Autorizzazione>(`getAutorizzazione uuid=${uuid}`))
    );
  }

  constructor(
    private http: HttpClient
  ) { }


  /** PUT: update the autorizzazione on the server */
  updateAutorizzazione (autorizzazione: Autorizzazione): Observable<any> {
    return this.http.put(this.autorizzazioniUrl, autorizzazione, httpOptions).pipe(
      catchError(this.handleError<any>('updateAutorizzazione'))
    );
  }

  /** POST: add a new autorizzazione to the server */
  addAutorizzazione (autorizzazione: Autorizzazione): Observable<Autorizzazione> {
    return this.http.post<Autorizzazione>(this.autorizzazioniUrl, autorizzazione, httpOptions).pipe(
      catchError(this.handleError<Autorizzazione>('addAutorizzazione'))
    );
  }

  /** DELETE: delete the autorizzazione from the server */
  deleteAutorizzazione (autorizzazione: Autorizzazione | number): Observable<Autorizzazione> {
    const uuid = typeof autorizzazione === 'number' ? autorizzazione : autorizzazione.uuid;
    const url = `${this.autorizzazioniUrl}/${uuid}`;

    return this.http.delete<Autorizzazione>(url, httpOptions).pipe(
      catchError(this.handleError<Autorizzazione>('deleteAutorizzazione'))
    );
  }

  /* GET autorizzazioni whose name contains search term */
  searchAutorizzazioni(term: string): Observable<Autorizzazione[]> {
    if (!term.trim()) {
      // if not search term, return empty autorizzazione array.
      return of([]);
    }
    return this.http.get<Autorizzazione[]>(`${this.autorizzazioniUrl}/search?uuid_progetto=${term}`).pipe(
      catchError(this.handleError<Autorizzazione[]>('searchAutorizzazioni', []))
    );
  }



   /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
