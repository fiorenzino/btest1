import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Attivita } from '../model/attivita';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ExportService {
  private exportUrl = '/api/v1/estrazioni';

  constructor(private http: HttpClient) { }

  csvExport(data_dal: Date, data_al: Date, email: string, progetto: string, cliente: string): Observable<string> {
    return this.http.get(
      `${this.exportUrl}/toCsv?data_dal=${data_dal}&data_al=${data_al}&email_sviluppatore=${email}
      &codice_progetto=${progetto}&codice_cliente=${cliente}`, { responseType: 'text' }
    );
  }

  jsonExport(data_dal: Date, data_al: Date, email: string, progetto: string, cliente: string): Observable<Attivita[]> {
    return this.http.get<Attivita[]>(
      `${this.exportUrl}/toJson?data_dal=${data_dal}&&data_al=${data_al}&&email_sviluppatore=${email}
      &&codice_progetto=${progetto}&&codice_cliente=${cliente}`)
      .pipe(catchError(this.handleError<Attivita[]>('jsonExport', []))
      );
  }

  excelExport(data_dal: Date, data_al: Date, email: string, progetto: string, cliente: string): Observable<Blob> {
    return this.http.get(
      `${this.exportUrl}/toExcel?data_dal=${data_dal}&&data_al=${data_al}&&email_sviluppatore=${email}
      &&codice_progetto=${progetto}&&codice_cliente=${cliente}`, { responseType: 'blob' }
    );
  }

  pdfExport(data_dal: Date, data_al: Date, email: string, progetto: string, cliente: string): Observable<Blob> {
    return this.http.get(
      `${this.exportUrl}/pdf?data_dal=${data_dal}&&data_al=${data_al}&&email_sviluppatore=${email}
      &&codice_progetto=${progetto}&&codice_cliente=${cliente}`, { responseType: 'blob' }
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead


      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
