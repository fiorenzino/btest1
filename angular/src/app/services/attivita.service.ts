import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {ATTIVITAS} from '../mock/mock-attivitas';
import { Attivita} from '../model/attivita';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class AttivitaService {
  private attivitasUrl = '/api/v1/attivita';  // URL to web api

  getAttivitas(params:Object):Observable<Attivita[]>{
    return this.http.get<Attivita[]>(this.attivitasUrl, params)
    .pipe(
      catchError(this.handleError('getAttivitas', [])))
  }

  getAttivita(uuid: string): Observable<Attivita> {
    const url = `${this.attivitasUrl}/${uuid}`;
    return this.http.get<Attivita>(url).pipe(
      catchError(this.handleError<Attivita>(`getAttivita uuid=${uuid}`))
    );
  }

  constructor(
    private http: HttpClient
  ) { }


  /** PUT: update the attivita on the server */
  updateAttivita (attivita: Attivita): Observable<any> {
    return this.http.put(`${this.attivitasUrl}/${attivita.uuid}`, attivita, httpOptions).pipe(
      catchError(this.handleError<any>('updateAttivita'))
    );
  }

  /** POST: add a new attivita to the server */
  addAttivita (attivita: Attivita): Observable<Attivita> {
    return this.http.post<Attivita>(this.attivitasUrl, attivita, httpOptions).pipe(
      catchError(this.handleError<Attivita>('addAttivita'))
    );
  }

  /** DELETE: delete the attivita from the server */
  deleteAttivita (attivita: Attivita | string): Observable<Attivita> {
    const uuid = typeof attivita === 'string' ? attivita : attivita.uuid;
    const url = `${this.attivitasUrl}/${uuid}`;

    return this.http.delete<Attivita>(url, httpOptions).pipe(
      catchError(this.handleError<Attivita>('deleteAttivita'))
    );
  }

  /* GET attivitas whose name contains search term */
  searchAttivitas(data_dal: Date, data_al: Date, email: string, progetto: string, cliente: string): Observable<Attivita[]> {
    return this.http.get<Attivita[]>(`${this.attivitasUrl}/search?data_dal=${data_dal}&&data_al=${data_al}&&email_sviluppatore=${email}
    &&codice_progetto=${progetto}&&codice_cliente=${cliente}`).pipe(
      catchError(this.handleError<Attivita[]>('searchAttivitas', []))
    );
  }

   /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
