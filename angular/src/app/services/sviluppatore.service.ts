import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {SVILUPPATORI} from '../mock/mock-sviluppatori';
import { Sviluppatore} from '../model/sviluppatore';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class SviluppatoreService {
  private sviluppatoriUrl = '/api/v1/sviluppatori';  // URL to web api

  getSviluppatori(params:Object):Observable<Sviluppatore[]>{
    return this.http.get<Sviluppatore[]>(this.sviluppatoriUrl, params)
    .pipe(
      catchError(this.handleError('getSviluppatori', [])))
  }

  getSviluppatore(uuid: string): Observable<Sviluppatore> {
    const url = `${this.sviluppatoriUrl}/${uuid}`;
    return this.http.get<Sviluppatore>(url).pipe(
      catchError(this.handleError<Sviluppatore>(`getSviluppatore uuid=${uuid}`))
    );
  }
  constructor(
    private http: HttpClient
  ) { }


  /** PUT: update the sviluppatore on the server */
  updateSviluppatore (sviluppatore: Sviluppatore): Observable<any> {
    return this.http.put(this.sviluppatoriUrl, sviluppatore, httpOptions).pipe(
      catchError(this.handleError<any>('updateSviluppatore'))
    );
  }

  /** POST: add a new sviluppatore to the server */
  addSviluppatore (sviluppatore: Sviluppatore): Observable<Sviluppatore> {
    return this.http.post<Sviluppatore>(this.sviluppatoriUrl, sviluppatore, httpOptions).pipe(
      catchError(this.handleError<Sviluppatore>('addSviluppatore'))
    );
  }

  /** DELETE: delete the sviluppatore from the server */
  deleteSviluppatore (sviluppatore: Sviluppatore | number): Observable<Sviluppatore> {
    const uuid = typeof sviluppatore === 'number' ? sviluppatore : sviluppatore.uuid;
    const url = `${this.sviluppatoriUrl}/${uuid}`;

    return this.http.delete<Sviluppatore>(url, httpOptions).pipe(
      catchError(this.handleError<Sviluppatore>('deleteSviluppatore'))
    );
  }

  /* GET sviluppatori whose name contains search term */
  searchSviluppatori(term: string): Observable<Sviluppatore[]> {
    if (!term.trim()) {
      // if not search term, return empty sviluppatore array.
      return of([]);
    }
    return this.http.get<Sviluppatore[]>(`${this.sviluppatoriUrl}/search?email=${term}`).pipe(
      catchError(this.handleError<Sviluppatore[]>('searchSviluppatori', []))
    );
  }



   /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
