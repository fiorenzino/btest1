import { Injectable } from '@angular/core';
import { Cliente } from '../model/cliente';
import { CLIENTI } from '../mock/mock-clienti';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class ClienteService {
  private clientiUrl = '/api/v1/clienti';  // URL to web api

  getClienti(params: Object): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.clientiUrl, params)
      .pipe(
        catchError(this.handleError('getClienti', [])))
  }

  getCliente(uuid: string): Observable<Cliente> {
    const url = `${this.clientiUrl}/${uuid}`;
    return this.http.get<Cliente>(url).pipe(
      catchError(this.handleError<Cliente>(`getCliente uuid=${uuid}`))
    );
  }

  constructor(
    private http: HttpClient
  ) { }


  /** PUT: update the cliente on the server */
  updateCliente(cliente: Cliente): Observable<any> {
    return this.http.put(this.clientiUrl, cliente, httpOptions).pipe(
      catchError(this.handleError<any>('updateCliente'))
    );
  }

  /** POST: add a new cliente to the server */
  addCliente(cliente: Cliente): Observable<Cliente> {
    return this.http.post<Cliente>(this.clientiUrl, cliente, httpOptions).pipe(
        catchError(this.handleError<Cliente>('addCliente'))
      );
  }

  /** DELETE: delete the cliente from the server */
  deleteCliente(cliente: Cliente | number): Observable<Cliente> {
    const uuid = typeof cliente === 'number' ? cliente : cliente.uuid;
    const url = `${this.clientiUrl}/${uuid}`;

    return this.http.delete<Cliente>(url, httpOptions).pipe(
      catchError(this.handleError<Cliente>('deleteCliente'))
    );
  }

  /* GET clienti whose name contains search term */
  searchClienti(term: string): Observable<Cliente[]> {
    if (!term.trim()) {
      // if not search term, return empty cliente array.
      return of([]);
    }
    return this.http.get<Cliente[]>(`${this.clientiUrl}/search?nome=${term}`).pipe(
      catchError(this.handleError<Cliente[]>('searchClienti', []))
    );
  }



  /**
  * Handle Http operation that failed.
  * Let the app continue.
  * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead


      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
