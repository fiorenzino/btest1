import { TestBed, inject } from '@angular/core/testing';

import { ProgettoService } from './progetto.service';

describe('ProgettoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProgettoService]
    });
  });

  it('should be created', inject([ProgettoService], (service: ProgettoService) => {
    expect(service).toBeTruthy();
  }));
});
