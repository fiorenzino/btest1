import { Progetto } from "./progetto";
import { Sviluppatore } from "./sviluppatore";

export class Autorizzazione {
    uuid: string;
    data_inizio: Date;
    data_fine: Date;
    uuid_progetto: string;
    progetto:Progetto;
    uuid_sviluppatore: string;
    sviluppatore: Sviluppatore;
    active: boolean;
  }