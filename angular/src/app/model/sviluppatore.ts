export class Sviluppatore {
    uuid: string;
    nome: string;
    cognome: string;
    email: string;
    active: boolean;
}