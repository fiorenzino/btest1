export class Attivita {
    uuid: string;
    data: Date;
    ore: number;
    descrizione: string;
    codice_progetto: string;
    email_sviluppatore: string;
    citta: string;
    uuid_uploadoperation: string;
    operatore: string;
    active: boolean;
  }