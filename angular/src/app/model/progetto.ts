import { Sviluppatore } from "./sviluppatore";
import { Cliente } from "./cliente";

export class Progetto {
    uuid: string;
    nome: string;
    codice: string;
    sviluppatore: Sviluppatore;
    responsabile: string;
    cliente: Cliente;
    uuid_cliente: string;
    active: boolean;
}