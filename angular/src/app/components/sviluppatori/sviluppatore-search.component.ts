import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
 
import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

import { Sviluppatore } from '../../model/sviluppatore';
import { SviluppatoreService } from '../../services/sviluppatore.service';

@Component({
  selector: 'app-sviluppatore-search',
  templateUrl: './sviluppatore-search.component.html',
  styleUrls: ['./sviluppatore-search.component.css']
})
export class SviluppatoreSearchComponent implements OnInit {

  sviluppatori$: Observable<Sviluppatore[]>;
  private searchTerms = new Subject<string>();
 
  constructor(private sviluppatoreService: SviluppatoreService) {}
 
  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }
 
  ngOnInit(): void {
    this.sviluppatori$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),
 
      // ignore new term if same as previous term
      distinctUntilChanged(),
 
      // switch to new search observable each time the term changes
      switchMap((term: string) => this.sviluppatoreService.searchSviluppatori(term)),
    );
  }

}
