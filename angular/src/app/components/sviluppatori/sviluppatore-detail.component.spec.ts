import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SviluppatoreDetailComponent } from './sviluppatore-detail.component';

describe('SviluppatoreDetailComponent', () => {
  let component: SviluppatoreDetailComponent;
  let fixture: ComponentFixture<SviluppatoreDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SviluppatoreDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SviluppatoreDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
