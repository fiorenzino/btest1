import { Component, OnInit } from '@angular/core';
import { Sviluppatore } from '../../model/sviluppatore';
import { SVILUPPATORI } from '../../mock/mock-sviluppatori';
import { SviluppatoreService } from '../../services/sviluppatore.service';
import {BrowserModule} from '@angular/platform-browser';
import {ConfirmationService, Message} from 'primeng/api';

@Component({
  selector: 'app-sviluppatori',
  templateUrl: './sviluppatori.component.html',
  styleUrls: ['./sviluppatori.component.css'],
  providers: [ConfirmationService],
})
export class SviluppatoriComponent implements OnInit {

  sviluppatori : Sviluppatore[];
  page : number = 0;
  pageSize : number = 10;
  msgs: Message[] = [];

  constructor(
    private sviluppatoreService: SviluppatoreService,
    private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.getSviluppatori();
  }

  getSviluppatori(): void{
    let requestParams = {
      params: {
        startRow: this.pageSize * this.page,
        pageSize: this.pageSize
      }
    }
    this.sviluppatoreService.getSviluppatori(requestParams).subscribe(sviluppatori => this.sviluppatori = sviluppatori);
  }

  delete(sviluppatore: Sviluppatore): void {
    this.sviluppatori = this.sviluppatori.filter(c => c !== sviluppatore);
    this.sviluppatoreService.deleteSviluppatore(sviluppatore).subscribe();
  }
  nextPage(): void {
    this.page += 1;
    this.getSviluppatori();
  }

  prevPage(): void {
    if (this.page >= 0){
      this.page -= 1;}
    this.getSviluppatori();
  }

  confirm2(attivita: Sviluppatore) {
    this.confirmationService.confirm({
        message: 'Do you want to delete this record?',
        header: 'Delete Confirmation',
        icon: 'pi pi-info-circle',
        accept: () => {
            this.msgs = [{severity:'info', summary:'Confirmed', detail:'Record deleted'}];
            this.delete(attivita);
            this.getSviluppatori();
        },
        reject: () => {
            this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
            this.getSviluppatori();
        }
    });
}


}
