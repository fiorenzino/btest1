import { Component, OnInit } from '@angular/core';
import { Sviluppatore } from '../../model/sviluppatore';
import { SVILUPPATORI } from '../../mock/mock-sviluppatori';
import { SviluppatoreService } from '../../services/sviluppatore.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-sviluppatore-add',
  templateUrl: './sviluppatore-add.component.html',
  styleUrls: ['./sviluppatore-add.component.css']
})
export class SviluppatoreAddComponent implements OnInit {
  sviluppatori : Sviluppatore[];

  constructor(
    private sviluppatoreService: SviluppatoreService,
    private location: Location
  ) { }

  ngOnInit() {
  }

  add(nome: string, cognome: string, email: string ): void {
    nome = nome.trim();
    if (!nome) { return; }
    cognome = cognome.trim();
    if (!cognome) { return; }
    email = email.trim();
    if (!email) { return; }
    this.sviluppatoreService.addSviluppatore({ nome, cognome, email } as Sviluppatore)
      .subscribe(sviluppatore => {
        this.sviluppatori.push(sviluppatore);
      });
  }

  goBack(): void {
    this.location.back();
  }

}
