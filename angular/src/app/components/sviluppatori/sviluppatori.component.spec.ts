import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SviluppatoriComponent } from './sviluppatori.component';

describe('SviluppatoriComponent', () => {
  let component: SviluppatoriComponent;
  let fixture: ComponentFixture<SviluppatoriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SviluppatoriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SviluppatoriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
