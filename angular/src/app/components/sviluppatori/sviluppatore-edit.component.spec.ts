import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SviluppatoreEditComponent } from './sviluppatore-edit.component';

describe('SviluppatoreEditComponent', () => {
  let component: SviluppatoreEditComponent;
  let fixture: ComponentFixture<SviluppatoreEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SviluppatoreEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SviluppatoreEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
