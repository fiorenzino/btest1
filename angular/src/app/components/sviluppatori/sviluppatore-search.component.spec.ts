import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SviluppatoreSearchComponent } from './sviluppatore-search.component';

describe('SviluppatoreSearchComponent', () => {
  let component: SviluppatoreSearchComponent;
  let fixture: ComponentFixture<SviluppatoreSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SviluppatoreSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SviluppatoreSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
