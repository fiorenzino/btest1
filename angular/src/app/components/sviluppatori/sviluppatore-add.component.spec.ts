import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SviluppatoreAddComponent } from './sviluppatore-add.component';

describe('SviluppatoreAddComponent', () => {
  let component: SviluppatoreAddComponent;
  let fixture: ComponentFixture<SviluppatoreAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SviluppatoreAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SviluppatoreAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
