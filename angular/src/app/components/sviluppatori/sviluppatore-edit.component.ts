import { Component, OnInit, Input } from '@angular/core';
import { Sviluppatore } from '../../model/sviluppatore';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { SviluppatoreService }  from '../../services/sviluppatore.service';

@Component({
  selector: 'app-sviluppatore-edit',
  templateUrl: './sviluppatore-edit.component.html',
  styleUrls: ['./sviluppatore-edit.component.css']
})
export class SviluppatoreEditComponent implements OnInit {

  @Input() sviluppatore: Sviluppatore;

  constructor(
    private route: ActivatedRoute,
    private sviluppatoreService: SviluppatoreService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getSviluppatore();
  }
 
  getSviluppatore(): void {
    const uuid = this.route.snapshot.paramMap.get('uuid');
    this.sviluppatoreService.getSviluppatore(uuid)
      .subscribe(sviluppatore => this.sviluppatore = sviluppatore);
  }
 
  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.sviluppatoreService.updateSviluppatore(this.sviluppatore)
      .subscribe(() => this.goBack());
  }

}
