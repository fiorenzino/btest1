import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizzazioneEditComponent } from './autorizzazione-edit.component';

describe('AutorizzazioneEditComponent', () => {
  let component: AutorizzazioneEditComponent;
  let fixture: ComponentFixture<AutorizzazioneEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizzazioneEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizzazioneEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
