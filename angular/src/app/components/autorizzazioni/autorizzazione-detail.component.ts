import { Component, OnInit, Input } from '@angular/core';
import { Autorizzazione } from '../../model/autorizzazione';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { AutorizzazioneService }  from '../../services/autorizzazione.service';

@Component({
  selector: 'app-autorizzazione-detail',
  templateUrl: './autorizzazione-detail.component.html',
  styleUrls: ['./autorizzazione-detail.component.css']
})
export class AutorizzazioneDetailComponent implements OnInit {

  @Input() autorizzazione: Autorizzazione;
  constructor(
    private route: ActivatedRoute,
    private autorizzazioneService: AutorizzazioneService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getAutorizzazione();
  }
 
  getAutorizzazione(): void {
    const uuid = this.route.snapshot.paramMap.get('uuid');
    this.autorizzazioneService.getAutorizzazione(uuid)
      .subscribe(autorizzazione => this.autorizzazione = autorizzazione);
  }
 
  goBack(): void {
    this.location.back();
  }

}
