import { Component, OnInit } from '@angular/core';
import { Autorizzazione } from '../../model/autorizzazione';
import { AUTORIZZAZIONI } from '../../mock/mock-autorizzazioni';
import { AutorizzazioneService } from '../../services/autorizzazione.service';
import { Progetto } from '../../model/progetto';
import { Sviluppatore } from '../../model/sviluppatore';
import { Location } from '@angular/common';

@Component({
  selector: 'app-autorizzazione-add',
  templateUrl: './autorizzazione-add.component.html',
  styleUrls: ['./autorizzazione-add.component.css']
})
export class AutorizzazioneAddComponent implements OnInit {

  autorizzazioni : Autorizzazione[];
  constructor(
    private autorizzazioneService: AutorizzazioneService,
    private location: Location
  ) { }

  ngOnInit() {
  }

  add(data_inizio: Date, data_fine: Date, uuid_progetto: string, progetto:Progetto, uuid_sviluppatore: string, sviluppatore: Sviluppatore): void {
    if (!data_inizio) { return; }
    if (!data_fine) { return; }
    this.autorizzazioneService.addAutorizzazione({ data_inizio, data_fine, uuid_progetto, progetto, uuid_sviluppatore, sviluppatore  } as Autorizzazione)
      .subscribe(autorizzazione => {
        this.autorizzazioni.push(autorizzazione);
      });
  }

  goBack(): void {
    this.location.back();
  }

}
