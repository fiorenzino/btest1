import { Component, OnInit } from '@angular/core';
import { Autorizzazione } from '../../model/autorizzazione';
import { AUTORIZZAZIONI } from '../../mock/mock-autorizzazioni';
import { AutorizzazioneService } from '../../services/autorizzazione.service';
import { Progetto } from '../../model/progetto';
import { Sviluppatore } from '../../model/sviluppatore';
import {BrowserModule} from '@angular/platform-browser';
import {ConfirmationService, Message} from 'primeng/api';

@Component({
  selector: 'app-autorizzazioni',
  templateUrl: './autorizzazioni.component.html',
  styleUrls: ['./autorizzazioni.component.css'],
  providers: [ConfirmationService],
})
export class AutorizzazioniComponent implements OnInit {

  autorizzazioni : Autorizzazione[];
  page : number = 0;
  pageSize : number = 10;
  msgs: Message[] = [];

  constructor(
    private autorizzazioneService: AutorizzazioneService,
    private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.getAutorizzazioni();
  }

  getAutorizzazioni(): void{
    let requestParams = {
      params: {
        startRow: this.pageSize * this.page,
        pageSize: this.pageSize
      }
    }
    this.autorizzazioneService.getAutorizzazioni(requestParams).subscribe(autorizzazioni => this.autorizzazioni = autorizzazioni);
  }

  delete(autorizzazione: Autorizzazione): void {
    this.autorizzazioni = this.autorizzazioni.filter(c => c !== autorizzazione);
    this.autorizzazioneService.deleteAutorizzazione(autorizzazione).subscribe();
  }

  nextPage(): void {
    this.page += 1;
    this.getAutorizzazioni();
  }

  prevPage(): void {
    if (this.page >= 0){
      this.page -= 1;}
    this.getAutorizzazioni();
  }

  confirm2(attivita: Autorizzazione) {
    this.confirmationService.confirm({
        message: 'Do you want to delete this record?',
        header: 'Delete Confirmation',
        icon: 'pi pi-info-circle',
        accept: () => {
            this.msgs = [{severity:'info', summary:'Confirmed', detail:'Record deleted'}];
            this.delete(attivita);
            this.getAutorizzazioni();
        },
        reject: () => {
            this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
            this.getAutorizzazioni();
        }
    });
}

}
