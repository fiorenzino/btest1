import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizzazioniComponent } from './autorizzazioni.component';

describe('AutorizzazioniComponent', () => {
  let component: AutorizzazioniComponent;
  let fixture: ComponentFixture<AutorizzazioniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizzazioniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizzazioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
