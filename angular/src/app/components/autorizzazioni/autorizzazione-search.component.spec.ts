import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizzazioneSearchComponent } from './autorizzazione-search.component';

describe('AutorizzazioneSearchComponent', () => {
  let component: AutorizzazioneSearchComponent;
  let fixture: ComponentFixture<AutorizzazioneSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizzazioneSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizzazioneSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
