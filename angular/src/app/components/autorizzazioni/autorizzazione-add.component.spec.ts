import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizzazioneAddComponent } from './autorizzazione-add.component';

describe('AutorizzazioneAddComponent', () => {
  let component: AutorizzazioneAddComponent;
  let fixture: ComponentFixture<AutorizzazioneAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizzazioneAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizzazioneAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
