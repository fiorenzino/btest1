import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizzazioneDetailComponent } from './autorizzazione-detail.component';

describe('AutorizzazioneDetailComponent', () => {
  let component: AutorizzazioneDetailComponent;
  let fixture: ComponentFixture<AutorizzazioneDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizzazioneDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizzazioneDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
