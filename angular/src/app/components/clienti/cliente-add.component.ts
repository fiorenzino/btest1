import { Component, OnInit } from '@angular/core';
import { Cliente } from '../../model/cliente';
import { CLIENTI } from '../../mock/mock-clienti';
import { ClienteService } from '../../services/cliente.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-cliente-add',
  templateUrl: './cliente-add.component.html',
  styleUrls: ['./cliente-add.component.css']
})
export class ClienteAddComponent implements OnInit {

  clienti : Cliente[];
  constructor(
    private clienteService: ClienteService,
    private location: Location) { }

  ngOnInit() {
  }

  add(nome: string): void {
    nome = nome.trim();
    if (!nome) { return; }
    this.clienteService.addCliente({ nome } as Cliente)
      .subscribe(cliente => {
        this.clienti.push(cliente);
      });
  }

  goBack(): void {
    this.location.back();
  }

}
