import { Component, OnInit } from '@angular/core';
import { Cliente } from '../../model/cliente';
import { CLIENTI } from '../../mock/mock-clienti';
import { ClienteService } from '../../services/cliente.service';
import {BrowserModule} from '@angular/platform-browser';
import {ConfirmationService, Message} from 'primeng/api';

@Component({
  selector: 'app-clienti',
  templateUrl: './clienti.component.html',
  styleUrls: ['./clienti.component.css'],
  providers: [ConfirmationService],
})
export class ClientiComponent implements OnInit {
  
  clienti : Cliente[];
  page : number = 0;
  pageSize : number = 10;
  msgs: Message[] = [];

  constructor(
    private clienteService: ClienteService,
    private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.getClienti();
  }

  getClienti(): void{
    let requestParams = {
      params: {
        startRow: this.pageSize * this.page,
        pageSize: this.pageSize
      }
    }
    this.clienteService.getClienti(requestParams).subscribe(clienti => this.clienti = clienti);
  }

  delete(cliente: Cliente): void {
    this.clienti = this.clienti.filter(c => c !== cliente);
    this.clienteService.deleteCliente(cliente).subscribe();
  }

  nextPage(): void {
    this.page += 1;
    this.getClienti();
  }

  prevPage(): void {
    if (this.page >= 0){
      this.page -= 1;}
    this.getClienti();
  }

  confirm2(attivita: Cliente) {
    this.confirmationService.confirm({
        message: 'Do you want to delete this record?',
        header: 'Delete Confirmation',
        icon: 'pi pi-info-circle',
        accept: () => {
            this.msgs = [{severity:'info', summary:'Confirmed', detail:'Record deleted'}];
            this.delete(attivita);
            this.getClienti();
        },
        reject: () => {
            this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
            this.getClienti();
        }
    });
}

}
