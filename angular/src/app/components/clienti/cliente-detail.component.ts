import { Component, OnInit, Input } from '@angular/core';
import { Cliente } from '../../model/cliente';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ClienteService }  from '../../services/cliente.service';

@Component({
  selector: 'app-cliente-detail',
  templateUrl: './cliente-detail.component.html',
  styleUrls: ['./cliente-detail.component.css']
})
export class ClienteDetailComponent implements OnInit {

  @Input() cliente: Cliente;
  
  constructor(
    private route: ActivatedRoute,
    private clienteService: ClienteService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getCliente();
  }
 
  getCliente(): void {
    const uuid = this.route.snapshot.paramMap.get('uuid');
    this.clienteService.getCliente(uuid)
      .subscribe(cliente => this.cliente = cliente);
  }
 
  goBack(): void {
    this.location.back();
  }


}
