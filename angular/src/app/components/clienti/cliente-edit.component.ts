import { Component, OnInit, Input } from '@angular/core';
import { Cliente } from '../../model/cliente';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ClienteService }  from '../../services/cliente.service';

@Component({
  selector: 'app-cliente-edit',
  templateUrl: './cliente-edit.component.html',
  styleUrls: ['./cliente-edit.component.css']
})
export class ClienteEditComponent implements OnInit {

  @Input() cliente: Cliente;
  
  constructor(
    private route: ActivatedRoute,
    private clienteService: ClienteService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getCliente();
  }
 
  getCliente(): void {
    const uuid = this.route.snapshot.paramMap.get('uuid');
    this.clienteService.getCliente(uuid)
      .subscribe(cliente => this.cliente = cliente);
  }
 
  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.clienteService.updateCliente(this.cliente)
      .subscribe(() => this.goBack());
  }

}
