import { Component, OnInit } from '@angular/core';
import { Attivita } from '../../model/attivita';
import { ATTIVITAS } from '../../mock/mock-attivitas';
import { AttivitaService } from '../../services/attivita.service';
import {BrowserModule} from '@angular/platform-browser';
import {ConfirmationService, Message} from 'primeng/api';


@Component({
  selector: 'app-attivitas',
  templateUrl: './attivitas.component.html',
  styleUrls: ['./attivitas.component.css'],
  providers: [ConfirmationService],

})
export class AttivitasComponent implements OnInit {

  attivitas : Attivita[];
  page : number = 0;
  pageSize : number = 10;
  msgs: Message[] = [];

  constructor(
    private attivitaService: AttivitaService,
    private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.getAttivitas();
  }

  getAttivitas(): void{
    let requestParams = {
      params: {
        startRow: this.pageSize * this.page,
        pageSize: this.pageSize
      }
    }
    this.attivitaService.getAttivitas(requestParams).subscribe(attivitas => this.attivitas = attivitas);
  }

  delete(attivita: Attivita): void {
    this.attivitas = this.attivitas.filter(c => c !== attivita);
    this.attivitaService.deleteAttivita(attivita).subscribe();
  }
  nextPage(): void {
    this.page += 1;
    this.getAttivitas();
  }

  prevPage(): void {
    if (this.page >= 0){
      this.page -= 1;}
    this.getAttivitas();
  }

  confirm2(attivita: Attivita) {
    this.confirmationService.confirm({
        message: 'Do you want to delete this record?',
        header: 'Delete Confirmation',
        icon: 'pi pi-info-circle',
        accept: () => {
            this.msgs = [{severity:'info', summary:'Confirmed', detail:'Record deleted'}];
            this.delete(attivita);
            this.getAttivitas();
        },
        reject: () => {
            this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
            this.getAttivitas();
        }
    });
}

}