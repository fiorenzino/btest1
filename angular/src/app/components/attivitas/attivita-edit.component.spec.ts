import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttivitaEditComponent } from './attivita-edit.component';

describe('AttivitaEditComponent', () => {
  let component: AttivitaEditComponent;
  let fixture: ComponentFixture<AttivitaEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttivitaEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttivitaEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
