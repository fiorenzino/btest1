import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttivitaSearchComponent } from './attivita-search.component';

describe('AttivitaSearchComponent', () => {
  let component: AttivitaSearchComponent;
  let fixture: ComponentFixture<AttivitaSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttivitaSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttivitaSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
