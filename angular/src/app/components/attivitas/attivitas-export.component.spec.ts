import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttivitasExportComponent } from './attivitas-export.component';

describe('AttivitasExportComponent', () => {
  let component: AttivitasExportComponent;
  let fixture: ComponentFixture<AttivitasExportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttivitasExportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttivitasExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
