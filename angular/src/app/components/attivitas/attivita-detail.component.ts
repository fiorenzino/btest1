import { Component, OnInit, Input } from '@angular/core';
import { Attivita } from '../../model/attivita';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { AttivitaService }  from '../../services/attivita.service';

@Component({
  selector: 'app-attivita-detail',
  templateUrl: './attivita-detail.component.html',
  styleUrls: ['./attivita-detail.component.css']
})
export class AttivitaDetailComponent implements OnInit {

  @Input() attivita: Attivita;

  constructor(
    private route: ActivatedRoute,
    private attivitaService: AttivitaService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getAttivita();
  }
 
  getAttivita(): void {
    const uuid = this.route.snapshot.paramMap.get('uuid');
    this.attivitaService.getAttivita(uuid)
      .subscribe(attivita => this.attivita = attivita);
  }
 
  goBack(): void {
    this.location.back();
  }

}
