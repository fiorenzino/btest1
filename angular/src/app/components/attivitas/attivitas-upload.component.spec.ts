import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttivitasUploadComponent } from './attivitas-upload.component';

describe('AttivitasUploadComponent', () => {
  let component: AttivitasUploadComponent;
  let fixture: ComponentFixture<AttivitasUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttivitasUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttivitasUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
