import {ViewChild, Component, NgModule, VERSION} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import { FileUpload } from 'primeng/primeng';

@Component({
  selector: 'app-attivitas-upload',
  templateUrl: './attivitas-upload.component.html',
})
export class AttivitasUploadComponent {

  @ViewChild('fileInput') fileInput: FileUpload;

  startUpload(){
    this.fileInput.upload();
  }

}
