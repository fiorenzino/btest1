import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttivitasComponent } from './attivitas.component';

describe('AttivitasComponent', () => {
  let component: AttivitasComponent;
  let fixture: ComponentFixture<AttivitasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttivitasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttivitasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
