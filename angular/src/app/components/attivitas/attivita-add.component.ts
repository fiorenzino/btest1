import { Component, OnInit } from '@angular/core';
import { Attivita } from '../../model/attivita';
import { AttivitaService } from '../../services/attivita.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-attivita-add',
  templateUrl: './attivita-add.component.html',
  styleUrls: ['./attivita-add.component.css']
})
export class AttivitaAddComponent implements OnInit {
  attivitas : Attivita[];

  constructor(
    private attivitaService: AttivitaService,
    private location: Location) { }

  ngOnInit() {
  }

  add(data: Date, ore: number, descrizione: string, codice_progetto: string, email_sviluppatore: string, citta: string, operatore: string): void {
    if (!data) { return; }
    if (!ore) { return; }
    codice_progetto = codice_progetto.trim();
    if (!codice_progetto) { return; }
    email_sviluppatore = email_sviluppatore.trim();
    if (!email_sviluppatore) { return; }
    this.attivitaService.addAttivita({ data, ore, descrizione, codice_progetto, email_sviluppatore, citta, operatore } as Attivita)
      .subscribe(attivita => {
        this.attivitas.push(attivita);
      });
  }

  goBack(): void {
    this.location.back();
  }

}
