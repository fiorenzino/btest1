import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
 
import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

import { Attivita } from '../../model/attivita';
import { AttivitaService } from '../../services/attivita.service';

@Component({
  selector: 'app-attivita-search',
  templateUrl: './attivita-search.component.html',
  styleUrls: ['./attivita-search.component.css']
})
export class AttivitaSearchComponent implements OnInit {

  attivitas$: Observable<Attivita[]>;
  private searchTerms = new Subject<string>();
 
  constructor(private attivitaService: AttivitaService) {}
 
  search(data_dal: Date, data_al: Date, email: string, progetto: string, cliente: string): void {
    this.attivitas$ = this.attivitaService.searchAttivitas(data_dal, data_al, email, progetto, cliente);
  }
 
  ngOnInit(): void {
 
  }

}
