import { Component, OnInit, Input } from '@angular/core';
import { Attivita } from '../../model/attivita';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { AttivitaService }  from '../../services/attivita.service';

@Component({
  selector: 'app-attivita-edit',
  templateUrl: './attivita-edit.component.html',
  styleUrls: ['./attivita-edit.component.css']
})
export class AttivitaEditComponent implements OnInit {

  @Input() attivita: Attivita;

  constructor(
    private route: ActivatedRoute,
    private attivitaService: AttivitaService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getAttivita();
  }
 
  getAttivita(): void {
    const uuid = this.route.snapshot.paramMap.get('uuid');
    this.attivitaService.getAttivita(uuid)
      .subscribe(attivita => this.attivita = attivita);
  }
 
  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.attivitaService.updateAttivita(this.attivita)
      .subscribe(() => this.goBack());
  }

}
