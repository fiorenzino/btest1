import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttivitaAddComponent } from './attivita-add.component';

describe('AttivitaAddComponent', () => {
  let component: AttivitaAddComponent;
  let fixture: ComponentFixture<AttivitaAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttivitaAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttivitaAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
