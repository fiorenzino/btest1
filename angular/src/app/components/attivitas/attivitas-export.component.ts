import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { saveAs } from 'file-saver';
import {ButtonModule} from 'primeng/button';

import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

import { Attivita } from '../../model/attivita';
import { ExportService } from '../../services/export.service';

@Component({
  selector: 'app-attivitas-export',
  templateUrl: './attivitas-export.component.html',
  styleUrls: ['./attivitas-export.component.css']
})
export class AttivitasExportComponent implements OnInit {

  attivitas: Attivita[];
  private searchTerms = new Subject<string>();

  constructor(private exportService: ExportService) { }

  csvSearch(data_dal: Date, data_al: Date, email: string, progetto: string, cliente: string): any {
    this.exportService.csvExport(data_dal, data_al, email, progetto, cliente)
      .subscribe(attivita => {
        saveAs(new Blob([attivita], { type: "text/csv;charset=utf-8;" }), 'file.csv')
      }
      );
  }

  jsonSearch(data_dal: Date, data_al: Date, email: string, progetto: string, cliente: string): void {
    this.exportService.jsonExport(data_dal, data_al, email, progetto, cliente)
      .subscribe(attivita => {
        var attivitas = JSON.stringify(attivita);
        saveAs(new Blob([attivitas], { type: "text/json;charset=utf-8;" }), 'file.json')
      }
      );
  }

  excelSearch(data_dal: Date, data_al: Date, email: string, progetto: string, cliente: string): void {
    this.exportService.excelExport(data_dal, data_al, email, progetto, cliente).subscribe(attivita => {
      saveAs(new Blob([attivita], { type: "text/xlsx;" }), 'file.xlsx')
    });
  }

  pdfSearch(data_dal: Date, data_al: Date, email: string, progetto: string, cliente: string): void {
    this.exportService.pdfExport(data_dal, data_al, email, progetto, cliente).subscribe(attivita => {
      saveAs(new Blob([attivita], { type: "text/pdf;" }), 'file.pdf')
    });
  }
  
  ngOnInit() {
  }

}
