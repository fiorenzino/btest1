import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {BrowserModule} from '@angular/platform-browser';
import {ConfirmationService, Message} from 'primeng/api';
 
import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

import { Progetto } from '../../model/progetto';
import { ProgettoService } from '../../services/progetto.service';

@Component({
  selector: 'app-progetto-search',
  templateUrl: './progetto-search.component.html',
  styleUrls: ['./progetto-search.component.css']
})
export class ProgettoSearchComponent implements OnInit {

  progetti$: Observable<Progetto[]>;
  private searchTerms = new Subject<string>();
 
  constructor(private progettoService: ProgettoService) {}
 
  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }
 
  ngOnInit(): void {
    this.progetti$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),
 
      // ignore new term if same as previous term
      distinctUntilChanged(),
 
      // switch to new search observable each time the term changes
      switchMap((term: string) => this.progettoService.searchProgetti(term)),
    );
  }

}
