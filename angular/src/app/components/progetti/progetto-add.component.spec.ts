import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgettoAddComponent } from './progetto-add.component';

describe('ProgettoAddComponent', () => {
  let component: ProgettoAddComponent;
  let fixture: ComponentFixture<ProgettoAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgettoAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgettoAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
