import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgettoSearchComponent } from './progetto-search.component';

describe('ProgettoSearchComponent', () => {
  let component: ProgettoSearchComponent;
  let fixture: ComponentFixture<ProgettoSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgettoSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgettoSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
