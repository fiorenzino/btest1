import { Component, OnInit } from '@angular/core';
import { Progetto } from '../../model/progetto';
import { PROGETTI } from '../../mock/mock-progetti';
import { ProgettoService } from '../../services/progetto.service';
import { Sviluppatore } from '../../model/sviluppatore';
import { Cliente } from '../../model/cliente';
import {BrowserModule} from '@angular/platform-browser';
import {ConfirmationService, Message} from 'primeng/api';

@Component({
  selector: 'app-progetti',
  templateUrl: './progetti.component.html',
  styleUrls: ['./progetti.component.css'],
  providers: [ConfirmationService],
})
export class ProgettiComponent implements OnInit {

  progetti : Progetto[];
  page : number = 0;
  pageSize : number = 10;
  msgs: Message[] = [];

  constructor(
    private progettoService: ProgettoService,
    private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.getProgetti();
  }

  getProgetti(): void{
    let requestParams = {
      params: {
        startRow: this.pageSize * this.page,
        pageSize: this.pageSize
      }
    }
    this.progettoService.getProgetti(requestParams).subscribe(progetti => this.progetti = progetti);
  }

  

  delete(progetto: Progetto): void {
    this.progetti = this.progetti.filter(c => c !== progetto);
    this.progettoService.deleteProgetto(progetto).subscribe();
  }

  nextPage(): void {
    this.page += 1;
    this.getProgetti();
  }

  prevPage(): void {
    if (this.page >= 0){
      this.page -= 1;}
    this.getProgetti();
  }

  confirm2(attivita: Progetto) {
    this.confirmationService.confirm({
        message: 'Do you want to delete this record?',
        header: 'Delete Confirmation',
        icon: 'pi pi-info-circle',
        accept: () => {
            this.msgs = [{severity:'info', summary:'Confirmed', detail:'Record deleted'}];
            this.delete(attivita);
            this.getProgetti();
        },
        reject: () => {
            this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
            this.getProgetti();
        }
    });
}
}
