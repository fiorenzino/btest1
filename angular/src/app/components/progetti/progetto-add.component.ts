import { Component, OnInit } from '@angular/core';
import { Progetto } from '../../model/progetto';
import { PROGETTI } from '../../mock/mock-progetti';
import { ProgettoService } from '../../services/progetto.service';
import { Sviluppatore } from '../../model/sviluppatore';
import { Cliente } from '../../model/cliente';
import { Location } from '@angular/common';

@Component({
  selector: 'app-progetto-add',
  templateUrl: './progetto-add.component.html',
  styleUrls: ['./progetto-add.component.css']
})
export class ProgettoAddComponent implements OnInit {
  progetti : Progetto[];

  constructor(
    private progettoService: ProgettoService,
    private location: Location
  ) { }

  ngOnInit() {
  }

  add(nome: string, codice: string, sviluppatore: Sviluppatore, responsabile: string, cliente: Cliente, uuid_cliente: string ): void {
    nome = nome.trim();
    if (!nome) { return; }
    codice = codice.trim();
    if (!codice) { return; }
    responsabile = responsabile.trim();
    if (!responsabile) { return; }
    uuid_cliente = uuid_cliente.trim();
    if (!uuid_cliente) { return; }
    this.progettoService.addProgetto({ nome, codice, responsabile, uuid_cliente } as Progetto)
      .subscribe(progetto => {
        this.progetti.push(progetto);
      });
  }

  goBack(): void {
    this.location.back();
  }

}
