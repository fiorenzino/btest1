import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgettoEditComponent } from './progetto-edit.component';

describe('ProgettoEditComponent', () => {
  let component: ProgettoEditComponent;
  let fixture: ComponentFixture<ProgettoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgettoEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgettoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
