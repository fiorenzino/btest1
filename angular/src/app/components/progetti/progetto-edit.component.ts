import { Component, OnInit, Input } from '@angular/core';
import { Progetto } from '../../model/progetto';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ProgettoService }  from '../../services/progetto.service';

@Component({
  selector: 'app-progetto-edit',
  templateUrl: './progetto-edit.component.html',
  styleUrls: ['./progetto-edit.component.css']
})
export class ProgettoEditComponent implements OnInit {

  @Input() progetto: Progetto;
  constructor(
    private route: ActivatedRoute,
    private progettoService: ProgettoService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getProgetto();
  }
 
  getProgetto(): void {
    const uuid = this.route.snapshot.paramMap.get('uuid');
    this.progettoService.getProgetto(uuid)
      .subscribe(progetto => this.progetto = progetto);
  }
 
  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.progettoService.updateProgetto(this.progetto)
      .subscribe(() => this.goBack());
  }

}
