package it.ictgroup.btest1.test.rs.management;

import static it.ictgroup.btest1.management.AppConstants.*;

public class AppConstants {
    public static final String HOST = "http://localhost:8080";

    public static final String ATTIVITA_TEST_PATH = HOST + API_PATH + ATTIVITA_PATH;
    public static final String AUTORIZZAZIONI_TEST_PATH = HOST + API_PATH + AUTORIZZAZIONI_PATH;
    public static final String CLIENTI_TEST_PATH = HOST + API_PATH + CLIENTI_PATH;
    public static final String ESTRAZIONI_TEST_PATH = HOST + API_PATH + ESTRAZIONI_PATH;
    public static final String PROGETTI_TEST_PATH = HOST + API_PATH + PROGETTI_PATH;
    public static final String SVILUPPATORI_TEST_PATH = HOST + API_PATH + SVILUPPATORI_PATH;
    public static final String UPLOAD_OPERATION_TEST_PATH = HOST + API_PATH + UPLOAD_OPERATIONS_PATH;

}
