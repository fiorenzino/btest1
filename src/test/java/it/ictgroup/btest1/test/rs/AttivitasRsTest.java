package it.ictgroup.btest1.test.rs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.ictgroup.btest1.model.*;
import it.ictgroup.btest1.test.rs.util.*;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;


public class AttivitasRsTest {
    Date date = new Date();


    @Test
    public void create() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        Sviluppatore sviluppatore = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com1");
        String uuid_s = SviluppatoriUtils.uuid(sviluppatore);
        sviluppatore.uuid = uuid_s;
        Cliente cliente = ClientiUtils.cliente("Mario");
        String uuid_c = ClientiUtils.uuid(cliente);
        cliente.uuid = uuid_c;
        Progetto progetto = ProgettiUtils.progetto("ProgettoProva", "P01", sviluppatore.email,sviluppatore, cliente.uuid, cliente);
        String uuid_p = ProgettiUtils.uuid(progetto);
        progetto.uuid =uuid_p;

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 3);
        Date dateEnd = c.getTime();

        Autorizzazione autorizzazione = AutorizzazioniUtils.autorizzazione(date, dateEnd, progetto, sviluppatore);
        String Autouuid = given()
                .contentType("application/json")
                .body(mapper.writeValueAsString(autorizzazione))
                .when()
                .post("http://localhost:8080/api/v1/autorizzazioni").
                        then().
                        statusCode(200).extract().
                        path("uuid");

        Attivita attivita = AttivitaUtils.attivita(date, 5, "desc", progetto.codice, sviluppatore.email);
        String uuid_a = AttivitaUtils.uuid(attivita);

        Attivita attivita_1 = AttivitaUtils.attivita(date, 8, "desc_2", progetto.codice, sviluppatore.email);
        String A1uuid = given()
                .contentType("application/json")
                .body(mapper.writeValueAsString(attivita_1))
                .when()
                .post("http://localhost:8080/api/v1/attivita").
                        then().
                        statusCode(400).extract().
                        path("uuid");

        AttivitaUtils.exist(uuid_a);
        ProgettiUtils.exist(uuid_p);
        SviluppatoriUtils.exist(uuid_s);


        AttivitaUtils.deleteHard(uuid_a);
        when().
                delete("http://localhost:8080/api/v1/autorizzazione/{id}", Autouuid).
                then().
                statusCode(404);
        ProgettiUtils.deleteHard(uuid_p);
        SviluppatoriUtils.deleteHard(uuid_s);

        AttivitaUtils.notExist(uuid_a);
        ProgettiUtils.notExist(uuid_p);
        SviluppatoriUtils.notExist(uuid_s);

    }


    @Test
    public void find() throws JsonProcessingException {
        Sviluppatore sviluppatore = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com");
        String uuid_s = SviluppatoriUtils.uuid(sviluppatore);
        sviluppatore.uuid = uuid_s;
        Cliente cliente = ClientiUtils.cliente("Mario");
        String uuid_c = ClientiUtils.uuid(cliente);
        cliente.uuid = uuid_c;
        Progetto progetto = ProgettiUtils.progetto("ProgettoProva", "P01", sviluppatore.email,sviluppatore, cliente.uuid, cliente);
        String uuid_p = ProgettiUtils.uuid(progetto);
        progetto.uuid =uuid_p;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 3);
        Date dateEnd = c.getTime();

        Autorizzazione autorizzazione = AutorizzazioniUtils.autorizzazione(date, dateEnd, progetto, sviluppatore);
        String Autouuid = given()
                .contentType("application/json")
                .body(autorizzazione)
                .when()
                .post("http://localhost:8080/api/v1/autorizzazioni").
                        then().
                        statusCode(200).extract().
                        path("uuid");
        Attivita attivita = AttivitaUtils.attivita(date, 5,"attivita1", "P01", "mario.rossi@gmail.com");
        String uuid_a = AttivitaUtils.uuid(attivita);

        AttivitaUtils.exist(uuid_a);
        ProgettiUtils.exist(uuid_p);
        SviluppatoriUtils.exist(uuid_s);

        AttivitaUtils.deleteHard(uuid_a);
        ProgettiUtils.deleteHard(uuid_p);
        SviluppatoriUtils.deleteHard(uuid_s);

        AttivitaUtils.notExist(uuid_a);
        ProgettiUtils.notExist(uuid_p);
        SviluppatoriUtils.notExist(uuid_s);
    }

    @Test
    public void list() throws JsonProcessingException{
        Sviluppatore sviluppatore_1 = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com");
        String uuid_s_1 = SviluppatoriUtils.uuid(sviluppatore_1);
        sviluppatore_1.uuid = uuid_s_1;
        Cliente cliente = ClientiUtils.cliente("Mario");
        String uuid_c = ClientiUtils.uuid(cliente);
        cliente.uuid = uuid_c;
        Progetto progetto_1 = ProgettiUtils.progetto("ProgettoProva", "P01", sviluppatore_1.email,sviluppatore_1, cliente.uuid, cliente);
        String uuid_p_1 = ProgettiUtils.uuid(progetto_1);
        progetto_1.uuid =uuid_p_1;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 3);
        Date dateEnd = c.getTime();

        Autorizzazione autorizzazione_1 = AutorizzazioniUtils.autorizzazione(date, dateEnd, progetto_1, sviluppatore_1);
        String Autouuid_1 = given()
                .contentType("application/json")
                .body(autorizzazione_1)
                .when()
                .post("http://localhost:8080/api/v1/autorizzazioni").
                        then().
                        statusCode(200).extract().
                        path("uuid");
        Attivita attivita_1 = AttivitaUtils.attivita(date, 5,"attivita1", "P01", "mario.rossi@gmail.com");
        String uuid_a_1 = AttivitaUtils.uuid(attivita_1);

        Sviluppatore sviluppatore_2 = SviluppatoriUtils.sviluppatore("Mario2", "Rossi2", "mario.rossi2@gmail.com");
        String uuid_s_2 = SviluppatoriUtils.uuid(sviluppatore_2);
        sviluppatore_2.uuid = uuid_s_2;
        Progetto progetto_2 = ProgettiUtils.progetto("ProgettoProva2", "P02", "mario.rossi2@gmail.com",sviluppatore_2, cliente.uuid, cliente);
        String uuid_p_2 = ProgettiUtils.uuid(progetto_2);
        progetto_2.uuid =uuid_p_2;

        Autorizzazione autorizzazione = AutorizzazioniUtils.autorizzazione(date, dateEnd, progetto_2, sviluppatore_2);
        String Autouuid_2 = given()
                .contentType("application/json")
                .body(autorizzazione)
                .when()
                .post("http://localhost:8080/api/v1/autorizzazioni").
                        then().
                        statusCode(200).extract().
                        path("uuid");
        Attivita attivita_2 = AttivitaUtils.attivita(date, 5,"attivita1", "P02", "mario.rossi2@gmail.com");
        String uuid_a_2 = AttivitaUtils.uuid(attivita_2);

        when().
                get("http://localhost:8080/api/v1/attivita").
                then().
                statusCode(200).header("listSize", Integer::parseInt, equalTo(2147483647));

        AttivitaUtils.exist(uuid_a_1);
        ProgettiUtils.exist(uuid_p_1);
        SviluppatoriUtils.exist(uuid_s_1);
        AttivitaUtils.exist(uuid_a_2);
        ProgettiUtils.exist(uuid_p_2);
        SviluppatoriUtils.exist(uuid_s_2);

        AttivitaUtils.deleteHard(uuid_a_1);
        ProgettiUtils.deleteHard(uuid_p_1);
        SviluppatoriUtils.deleteHard(uuid_s_1);
        AttivitaUtils.deleteHard(uuid_a_2);
        ProgettiUtils.deleteHard(uuid_p_2);
        SviluppatoriUtils.deleteHard(uuid_s_2);

        AttivitaUtils.notExist(uuid_a_1);
        ProgettiUtils.notExist(uuid_p_1);
        SviluppatoriUtils.notExist(uuid_s_1);
        AttivitaUtils.notExist(uuid_a_2);
        ProgettiUtils.notExist(uuid_p_2);
        SviluppatoriUtils.notExist(uuid_s_2);
    }

    @Test
    public void delete() throws JsonProcessingException{

        Sviluppatore sviluppatore = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com");
        String uuid_s = SviluppatoriUtils.uuid(sviluppatore);
        sviluppatore.uuid = uuid_s;
        Cliente cliente = ClientiUtils.cliente("Mario");
        String uuid_c = ClientiUtils.uuid(cliente);
        cliente.uuid = uuid_c;
        Progetto progetto = ProgettiUtils.progetto("ProgettoProva", "P01", sviluppatore.email,sviluppatore, cliente.uuid, cliente);
        String uuid_p = ProgettiUtils.uuid(progetto);
        progetto.uuid =uuid_p;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 3);
        Date dateEnd = c.getTime();

        Autorizzazione autorizzazione = AutorizzazioniUtils.autorizzazione(date, dateEnd, progetto, sviluppatore);
        String Autouuid = given()
                .contentType("application/json")
                .body(autorizzazione)
                .when()
                .post("http://localhost:8080/api/v1/autorizzazioni").
                        then().
                        statusCode(200).extract().
                        path("uuid");
        Attivita attivita = AttivitaUtils.attivita(date, 5,"attivita1", "P01", "mario.rossi@gmail.com");
        String uuid_a = AttivitaUtils.uuid(attivita);

        AttivitaUtils.deleteHard(uuid_a);
        ProgettiUtils.deleteHard(uuid_p);
        SviluppatoriUtils.deleteHard(uuid_s);

        AttivitaUtils.notExist(uuid_a);
        ProgettiUtils.notExist(uuid_p);
        SviluppatoriUtils.notExist(uuid_s);
    }

}