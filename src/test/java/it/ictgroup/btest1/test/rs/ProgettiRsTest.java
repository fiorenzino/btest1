package it.ictgroup.btest1.test.rs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.ictgroup.btest1.model.Cliente;
import it.ictgroup.btest1.model.Progetto;
import it.ictgroup.btest1.model.Sviluppatore;
import it.ictgroup.btest1.test.rs.util.ClientiUtils;
import it.ictgroup.btest1.test.rs.util.ProgettiUtils;
import it.ictgroup.btest1.test.rs.util.SviluppatoriUtils;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.apache.commons.lang3.StringUtils.containsOnly;
import static org.hamcrest.Matchers.*;


public class ProgettiRsTest {


    @Test
    public void create() throws JsonProcessingException {
        Sviluppatore sviluppatore = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com");
        String uuid_s = SviluppatoriUtils.uuid(sviluppatore);
        sviluppatore.uuid = uuid_s;
        Cliente cliente = ClientiUtils.cliente("Mario");
        String uuid_c = ClientiUtils.uuid(cliente);
        cliente.uuid = uuid_c;
        Progetto progetto = ProgettiUtils.progetto("ProgettoProva", "P01", "mario.rossi@gmail.com",sviluppatore, cliente.uuid, cliente);
        String uuid_p = ProgettiUtils.uuid(progetto);

        ProgettiUtils.exist(uuid_p);
        ClientiUtils.exist(uuid_c);
        SviluppatoriUtils.exist(uuid_s);

        ProgettiUtils.deleteHard(uuid_p);
        ClientiUtils.deleteHard(uuid_c);
        SviluppatoriUtils.deleteHard(uuid_s);

        ProgettiUtils.notExist(uuid_p);
        ClientiUtils.notExist(uuid_c);
        SviluppatoriUtils.notExist(uuid_s);
    }


    @Test
    public void find() throws JsonProcessingException {
        Sviluppatore sviluppatore = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com");
        String uuid_s = SviluppatoriUtils.uuid(sviluppatore);
        sviluppatore.uuid = uuid_s;
        Cliente cliente = ClientiUtils.cliente("Mario");
        String uuid_c = ClientiUtils.uuid(cliente);
        cliente.uuid = uuid_c;
        Progetto progetto = ProgettiUtils.progetto("ProgettoProva", "P01", "mario.rossi@gmail.com",sviluppatore, cliente.uuid, cliente);
        String uuid_p = ProgettiUtils.uuid(progetto);

        ProgettiUtils.exist(uuid_p);
        ClientiUtils.exist(uuid_c);
        SviluppatoriUtils.exist(uuid_s);

        ProgettiUtils.deleteHard(uuid_p);
        ClientiUtils.deleteHard(uuid_c);
        SviluppatoriUtils.deleteHard(uuid_s);

        ProgettiUtils.notExist(uuid_p);
        ClientiUtils.notExist(uuid_c);
        SviluppatoriUtils.notExist(uuid_s);

    }

    @Test
    public void list() throws JsonProcessingException{
        Sviluppatore sviluppatore_1 = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com");
        String uuid_s_1 = SviluppatoriUtils.uuid(sviluppatore_1);
        sviluppatore_1.uuid = uuid_s_1;
        Cliente cliente = ClientiUtils.cliente("Mario");
        String uuid_c = ClientiUtils.uuid(cliente);
        cliente.uuid = uuid_c;
        Progetto progetto_1 = ProgettiUtils.progetto("ProgettoProva", "P01", "mario.rossi@gmail.com",sviluppatore_1,cliente.uuid, cliente);
        String uuid_p_1 = ProgettiUtils.uuid(progetto_1);

        Sviluppatore sviluppatore_2 = SviluppatoriUtils.sviluppatore("Mario2", "Rossi2", "mario2.rossi2@gmail.com");
        String uuid_s_2 = SviluppatoriUtils.uuid(sviluppatore_2);
        sviluppatore_2.uuid = uuid_s_2;
        Progetto progetto_2 = ProgettiUtils.progetto("ProgettoProva2", "P02", "mario2.rossi2@gmail.com",sviluppatore_2, cliente.uuid, cliente);
        String uuid_p_2 = ProgettiUtils.uuid(progetto_2);

        when().
                get("http://localhost:8080/api/v1/progetti").
                then().
                statusCode(200).header("listSize", Integer::parseInt, equalTo(2));

        ProgettiUtils.exist(uuid_p_1);
        SviluppatoriUtils.exist(uuid_s_1);
        ProgettiUtils.exist(uuid_p_2);
        SviluppatoriUtils.exist(uuid_s_2);

        ProgettiUtils.deleteHard(uuid_p_1);
        SviluppatoriUtils.deleteHard(uuid_s_1);
        ProgettiUtils.deleteHard(uuid_p_2);
        SviluppatoriUtils.deleteHard(uuid_s_2);
        ClientiUtils.deleteHard(uuid_c);

        ProgettiUtils.notExist(uuid_p_1);
        SviluppatoriUtils.notExist(uuid_s_1);
        ProgettiUtils.notExist(uuid_p_2);
        SviluppatoriUtils.notExist(uuid_s_2);
    }

    @Test
    public void delete() throws JsonProcessingException{
        Sviluppatore sviluppatore = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com");
        String uuid_s = SviluppatoriUtils.uuid(sviluppatore);
        sviluppatore.uuid = uuid_s;
        Cliente cliente = ClientiUtils.cliente("Mario");
        String uuid_c = ClientiUtils.uuid(cliente);
        cliente.uuid = uuid_c;
        Progetto progetto = ProgettiUtils.progetto("ProgettoProva", "P01", "mario.rossi@gmail.com",sviluppatore, cliente.uuid, cliente);
        String uuid_p = ProgettiUtils.uuid(progetto);

        ProgettiUtils.deleteHard(uuid_p);
        ClientiUtils.deleteHard(uuid_c);
        SviluppatoriUtils.deleteHard(uuid_s);

        ProgettiUtils.notExist(uuid_p);
        ClientiUtils.notExist(uuid_c);
        SviluppatoriUtils.notExist(uuid_s);
    }

}
