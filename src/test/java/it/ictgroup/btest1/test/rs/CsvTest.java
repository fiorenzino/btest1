package it.ictgroup.btest1.test.rs;

import it.ictgroup.btest1.test.rs.management.AppConstants;
import org.junit.Test;

import java.io.File;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.Is.is;

public class CsvTest {

    @Test
    public void upload() {
        System.out.println(AppConstants.ATTIVITA_TEST_PATH);
        given().
                multiPart("file", new File("docs/prova.csv")).
                expect().
                body("file", is("OK")).
                when().
                post(AppConstants.ATTIVITA_TEST_PATH + "/massive");
    }
}
