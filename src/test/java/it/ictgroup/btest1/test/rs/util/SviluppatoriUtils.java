package it.ictgroup.btest1.test.rs.util;

import it.ictgroup.btest1.model.Autorizzazione;
import it.ictgroup.btest1.model.Progetto;
import it.ictgroup.btest1.model.Sviluppatore;

import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

public class SviluppatoriUtils {

    public static Sviluppatore sviluppatore(String nome, String cognome, String email){
        Sviluppatore sviluppatore = new Sviluppatore();
        sviluppatore.cognome = cognome;
        sviluppatore.nome = nome;
        sviluppatore.email = email;
        return sviluppatore;
    }

    public static String uuid(Sviluppatore sviluppatore){
        return  given()
                .contentType("application/json")
                .body(sviluppatore)
                .when()
                .post("http://localhost:8080/api/v1/sviluppatori")
                .then()
                .statusCode(200).extract().path("uuid");
    }

    public static void exist(String uuid){
        when().
                get("http://localhost:8080/api/v1/sviluppatori/{id}", uuid).
                then().statusCode(200).assertThat().body("active",equalTo(true));
    }
    public static void deleteHard (String uuid){
        when().
                delete("http://localhost:8080/api/v1/sviluppatori/hard/{id}", uuid).
                then().
                statusCode(204);
    }

    public static void notExist(String uuid){
        given().pathParam("id", uuid)
                .when().get("http://localhost:8080/api/v1/sviluppatori/{id}").then()
                .statusCode(200).assertThat().body(equalTo(""));
    }
}
