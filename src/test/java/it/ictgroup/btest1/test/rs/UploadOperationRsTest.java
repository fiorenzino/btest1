package it.ictgroup.btest1.test.rs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.ictgroup.btest1.model.*;
import it.ictgroup.btest1.test.rs.util.*;
import it.ictgroup.btest1.util.FileUtils;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static io.restassured.RestAssured.given;
import static it.ictgroup.btest1.test.rs.management.AppConstants.UPLOAD_OPERATION_TEST_PATH;

public class UploadOperationRsTest {
    @Test
    public void test() throws IOException {
//        Date date = new Date();
//        ObjectMapper mapper = new ObjectMapper();
//        Sviluppatore sviluppatore = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com");
//        String uuid_s = SviluppatoriUtils.uuid(sviluppatore);
//        sviluppatore.uuid = uuid_s;
//        Cliente cliente = ClientiUtils.cliente("Mario");
//        String uuid_c = ClientiUtils.uuid(cliente);
//        cliente.uuid = uuid_c;
//        Progetto progetto = ProgettiUtils.progetto("ProgettoProva", "P01", sviluppatore.email, sviluppatore, cliente.uuid, cliente);
//        String uuid_p = ProgettiUtils.uuid(progetto);
//        progetto.uuid = uuid_p;
//
//        Calendar c = Calendar.getInstance();
//        c.setTime(date);
//        c.add(Calendar.DATE, 100);
//        Date dateEnd = c.getTime();
//
//        Autorizzazione autorizzazione = AutorizzazioniUtils.autorizzazione(date, dateEnd, progetto, sviluppatore);
//        String Autouuid = AutorizzazioniUtils.uuid(autorizzazione);

        String content = new String(Files.readAllBytes(Paths.get("06.prandini_maggio.csv")));
        File file = new File("06.prandini_maggio.csv");

//        given()
//                .multiPart("file", file)
//                .when()
//                .post(UPLOAD_OPERATION_TEST_PATH + "/queue").
//                then().
//                statusCode(200);

//        given()
//                .multiPart("file", file)
//                .when()
//                .post(UPLOAD_OPERATION_TEST_PATH + "/async").
//                then().
//                statusCode(200);

        given()
                .multiPart("file", file)
                .when()
                .post(UPLOAD_OPERATION_TEST_PATH + "/queue").
                then().
                statusCode(200);


    }
}
