package it.ictgroup.btest1.test.rs.util;

import it.ictgroup.btest1.model.Cliente;
import it.ictgroup.btest1.model.Progetto;
import it.ictgroup.btest1.model.Sviluppatore;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

public class ProgettiUtils {
    public static Progetto progetto(String nome, String codice, String responsabile, Sviluppatore sviluppatore, String uuid, Cliente cliente){
        Progetto progetto = new Progetto();
        progetto.nome = nome;
        progetto.codice = codice;
        progetto.responsabile = responsabile;
        progetto.sviluppatore= sviluppatore;
        progetto.uuid_cliente = uuid;
        progetto.cliente= cliente;

        return progetto;
    }

    public static String uuid(Progetto progetto){
        return  given()
                .contentType("application/json")
                .body(progetto)
                .when()
                .post("http://localhost:8080/api/v1/progetti")
                .then()
                .statusCode(200).extract().path("uuid");
    }

    public static void exist(String uuid){
        when().
                get("http://localhost:8080/api/v1/progetti/{id}", uuid).
                then().statusCode(200).assertThat().body("active",equalTo(true));
    }
    public static void deleteHard (String uuid){
        when().
                delete("http://localhost:8080/api/v1/progetti/hard/{id}", uuid).
                then().
                statusCode(204);
    }

    public static void notExist(String uuid){
        given().pathParam("id", uuid)
                .when().get("http://localhost:8080/api/v1/progetti/{id}").then()
                .statusCode(200).assertThat().body(equalTo(""));
    }
}
