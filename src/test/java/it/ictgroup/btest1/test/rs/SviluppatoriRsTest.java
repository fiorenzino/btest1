package it.ictgroup.btest1.test.rs;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.ictgroup.btest1.model.Sviluppatore;
import it.ictgroup.btest1.test.rs.util.SviluppatoriUtils;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.apache.commons.lang3.StringUtils.containsOnly;
import static org.hamcrest.Matchers.*;


public class SviluppatoriRsTest {

    @Test
    public void create() throws JsonProcessingException {
        Sviluppatore sviluppatore = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com");
        String uuid = SviluppatoriUtils.uuid(sviluppatore);
        SviluppatoriUtils.exist(uuid);
        SviluppatoriUtils.deleteHard(uuid);
        SviluppatoriUtils.notExist(uuid);
    }


    @Test
    public void find() throws JsonProcessingException {
        Sviluppatore sviluppatore = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com");
        String uuid = SviluppatoriUtils.uuid(sviluppatore);
        SviluppatoriUtils.exist(uuid);
        SviluppatoriUtils.deleteHard(uuid);
        SviluppatoriUtils.notExist(uuid);
    }

    @Test
    public void list() throws JsonProcessingException {
        Sviluppatore sviluppatore_1 = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com");
        String uuid_1 = SviluppatoriUtils.uuid(sviluppatore_1);

        Sviluppatore sviluppatore_2 = SviluppatoriUtils.sviluppatore("Mario1", "Rossi1", "mario1.rossi1@gmail.com");
        String uuid_2 = SviluppatoriUtils.uuid(sviluppatore_2);

        when().
                get("http://localhost:8080/api/v1/sviluppatori").
                then().
                statusCode(200).header("listSize", Integer::parseInt, equalTo(2147483647));

        SviluppatoriUtils.exist(uuid_1);
        SviluppatoriUtils.exist(uuid_2);

        SviluppatoriUtils.deleteHard(uuid_1);
        SviluppatoriUtils.deleteHard(uuid_2);

        SviluppatoriUtils.notExist(uuid_1);
        SviluppatoriUtils.notExist(uuid_2);
    }

    @Test
    public void delete() throws JsonProcessingException {
        Sviluppatore sviluppatore = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com");
        String uuid = SviluppatoriUtils.uuid(sviluppatore);
        SviluppatoriUtils.deleteHard(uuid);
        SviluppatoriUtils.notExist(uuid);


    }


}