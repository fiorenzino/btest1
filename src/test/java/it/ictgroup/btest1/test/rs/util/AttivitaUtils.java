package it.ictgroup.btest1.test.rs.util;

import it.ictgroup.btest1.model.Attivita;
import it.ictgroup.btest1.model.Sviluppatore;

import java.util.Date;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

public class AttivitaUtils {


    public static Attivita attivita(Date data, int ore, String descrizione, String codice, String email) {
        Attivita attivita = new Attivita();

        attivita.data = data;
        attivita.ore = ore;
        attivita.descrizione = descrizione;
        attivita.codice_progetto = codice;
        attivita.email_sviluppatore = email;
        return attivita;

    }

    public static String uuid(Attivita attivita){
        return  given()
                .contentType("application/json")
                .body(attivita)
                .when()
                .post("http://localhost:8080/api/v1/attivita")
                .then()
                .statusCode(200).extract().path("uuid");
    }

    public static void exist(String uuid){
        when().
                get("http://localhost:8080/api/v1/attivita/{id}", uuid).
                then().statusCode(200).assertThat().body("active",equalTo(true));
    }
    public static void deleteHard (String uuid){
        when().
                delete("http://localhost:8080/api/v1/attivita/hard/{id}", uuid).
                then().
                statusCode(204);
    }

    public static void notExist(String uuid){
        given().pathParam("id", uuid)
                .when().get("http://localhost:8080/api/v1/attivita/{id}").then()
                .statusCode(200).assertThat().body(equalTo(""));
    }
}
