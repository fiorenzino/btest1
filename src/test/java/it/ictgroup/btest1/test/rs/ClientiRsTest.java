package it.ictgroup.btest1.test.rs;

import com.fasterxml.jackson.core.JsonProcessingException;
import it.ictgroup.btest1.model.Cliente;
import it.ictgroup.btest1.test.rs.util.ClientiUtils;
import org.junit.Test;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

public class ClientiRsTest {

    @Test
    public void create() throws JsonProcessingException {
        Cliente cliente = ClientiUtils.cliente("Mario");
        String uuid = ClientiUtils.uuid(cliente);
        ClientiUtils.exist(uuid);
        ClientiUtils.deleteHard(uuid);
        ClientiUtils.notExist(uuid);
    }


    @Test
    public void find() throws JsonProcessingException {
        Cliente cliente = ClientiUtils.cliente("Mario");
        String uuid = ClientiUtils.uuid(cliente);
        ClientiUtils.exist(uuid);
        ClientiUtils.deleteHard(uuid);
        ClientiUtils.notExist(uuid);
    }

    @Test
    public void list() throws JsonProcessingException {
        Cliente cliente_1 = ClientiUtils.cliente("Mario");
        String uuid_1 = ClientiUtils.uuid(cliente_1);

        Cliente cliente_2 = ClientiUtils.cliente("Mario1");
        String uuid_2 = ClientiUtils.uuid(cliente_2);

        when().
                get("http://localhost:8080/api/v1/sviluppatori").
                then().
                statusCode(200).header("listSize", Integer::parseInt, equalTo(2147483647));

        ClientiUtils.exist(uuid_1);
        ClientiUtils.exist(uuid_2);

        ClientiUtils.deleteHard(uuid_1);
        ClientiUtils.deleteHard(uuid_2);

        ClientiUtils.notExist(uuid_1);
        ClientiUtils.notExist(uuid_2);
    }

    @Test
    public void delete() throws JsonProcessingException {
        Cliente cliente = ClientiUtils.cliente("Mario");
        String uuid = ClientiUtils.uuid(cliente);
        ClientiUtils.deleteHard(uuid);
        ClientiUtils.notExist(uuid);


    }
}
