package it.ictgroup.btest1.test.rs.util;

import it.ictgroup.btest1.model.Autorizzazione;
import it.ictgroup.btest1.model.Progetto;
import it.ictgroup.btest1.model.Sviluppatore;

import java.util.Calendar;
import java.util.Date;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

public class AutorizzazioniUtils {

    public static Autorizzazione autorizzazione(Date data_inizio, Date data_fine, Progetto progetto, Sviluppatore sviluppatore){
        Autorizzazione autorizzazione = new Autorizzazione();
        autorizzazione.data_inizio = data_inizio;
        autorizzazione.data_fine = data_fine;
        autorizzazione.uuid_progetto = progetto.uuid;
        autorizzazione.uuid_sviluppatore = sviluppatore.uuid;
        autorizzazione.progetto = progetto;
        autorizzazione.sviluppatore = sviluppatore;
        return autorizzazione;
    }

    public static String uuid(Autorizzazione autorizzazione){
        return  given()
                .contentType("application/json")
                .body(autorizzazione)
                .when()
                .post("http://localhost:8080/api/v1/autorizzazioni")
                .then()
                .statusCode(200).extract().path("uuid");
    }

    public static void exist(String uuid){
        when().
                get("http://localhost:8080/api/v1/autorizzazioni/{id}", uuid).
                then().statusCode(200).assertThat().body("active",equalTo(true));
    }
    public static void deleteHard (String uuid){
        when().
                delete("http://localhost:8080/api/v1/autorizzazioni/{id}", uuid).
                then().
                statusCode(204); //perchè non cè una delete_hard per autorizzazioni
    }

    public static void notExist(String uuid){
        given().pathParam("id", uuid)
                .when().get("http://localhost:8080/api/v1/autorizzazioni/{id}").then()
                .statusCode(200).assertThat().body(equalTo(""));
    }
}
