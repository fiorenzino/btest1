package it.ictgroup.btest1.test.rs.util;

import it.ictgroup.btest1.model.Cliente;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

public class ClientiUtils {

    public static Cliente cliente(String nome){
        Cliente cliente = new Cliente();
        cliente.nome = nome;
        return cliente;
    }

    public static String uuid(Cliente cliente){
        return  given()
                .contentType("application/json")
                .body(cliente)
                .when()
                .post("http://localhost:8080/api/v1/clienti")
                .then()
                .statusCode(200).extract().path("uuid");
    }

    public static void exist(String uuid){
        when().
                get("http://localhost:8080/api/v1/clienti/{id}", uuid).
                then().statusCode(200).assertThat().body("active",equalTo(true));
    }
    public static void deleteHard (String uuid){
        when().
                delete("http://localhost:8080/api/v1/clienti/hard/{id}", uuid).
                then().
                statusCode(204);
    }

    public static void notExist(String uuid){
        given().pathParam("id", uuid)
                .when().get("http://localhost:8080/api/v1/clienti/{id}").then()
                .statusCode(200).assertThat().body(equalTo(""));
    }
}
