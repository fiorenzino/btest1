package it.ictgroup.btest1.test.rs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.ictgroup.btest1.model.*;
import it.ictgroup.btest1.test.rs.util.*;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static io.restassured.RestAssured.given;

public class EstrazioniRsTest {

    Date date = new Date();

    @Test
    public void create() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        Sviluppatore sviluppatore = SviluppatoriUtils.sviluppatore("Mario", "Rossi", "mario.rossi@gmail.com");
        String uuid_s = SviluppatoriUtils.uuid(sviluppatore);
        sviluppatore.uuid = uuid_s;
        Cliente cliente = ClientiUtils.cliente("Mario");
        String uuid_c = ClientiUtils.uuid(cliente);
        cliente.uuid = uuid_c;
        Progetto progetto = ProgettiUtils.progetto("ProgettoProva", "P01", sviluppatore.email, sviluppatore, cliente.uuid, cliente);
        String uuid_p = ProgettiUtils.uuid(progetto);
        progetto.uuid = uuid_p;

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 3);
        Date dateA = c.getTime();
        c.add(Calendar.DATE, 1);
        Date dateB = c.getTime();
        c.add(Calendar.DATE, 3);
        Date dateEnd = c.getTime();

        Autorizzazione autorizzazione = AutorizzazioniUtils.autorizzazione(date, dateEnd, progetto, sviluppatore);
        String Autouuid = AutorizzazioniUtils.uuid(autorizzazione);

        Attivita attivita = AttivitaUtils.attivita(dateA, 5, "desc", progetto.codice, sviluppatore.email);
        String uuid_a = AttivitaUtils.uuid(attivita);

        Attivita attivita_2 = AttivitaUtils.attivita(dateB, 5, "desc", progetto.codice, sviluppatore.email);
        String uuid_a_2 = AttivitaUtils.uuid(attivita_2);

        AttivitaUtils.exist(uuid_a);
        AutorizzazioniUtils.exist(Autouuid);
        ClientiUtils.exist(uuid_c);
        ProgettiUtils.exist(uuid_p);
        SviluppatoriUtils.exist(uuid_s);
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

        given().queryParams("data_dal", df.format(autorizzazione.data_inizio)).queryParam("data_al", df.format(autorizzazione.data_fine))
                .queryParam("email_sviluppatore", sviluppatore.email).queryParam("codice_progetto", progetto.codice)
                .queryParam("codice_cliente", cliente.uuid)
                .when()
                .get("http://localhost:8080/api/v1/estrazioni/toJson")
                .then()
                .statusCode(200);

        given().queryParams("data_dal", df.format(autorizzazione.data_inizio)).queryParam("data_al", df.format(autorizzazione.data_fine))
                .queryParam("email_sviluppatore", sviluppatore.email).queryParam("codice_progetto", progetto.codice)
                .queryParam("codice_cliente", cliente.uuid)
                .when()
                .get("http://localhost:8080/api/v1/estrazioni/toCsv")
                .then()
                .statusCode(200);

        given().queryParams("data_dal", df.format(autorizzazione.data_inizio)).queryParam("data_al", df.format(autorizzazione.data_fine))
                .queryParam("email_sviluppatore", sviluppatore.email).queryParam("codice_progetto", progetto.codice)
                .queryParam("codice_cliente", cliente.uuid)
                .when()
                .get("http://localhost:8080/api/v1/estrazioni/toExcel").
                then().statusCode(200);

        AttivitaUtils.exist(uuid_a);
        ProgettiUtils.exist(uuid_p);
        SviluppatoriUtils.exist(uuid_s);

        AttivitaUtils.deleteHard(uuid_a);
        AttivitaUtils.deleteHard(uuid_a_2);
        AutorizzazioniUtils.deleteHard(Autouuid);
        ProgettiUtils.deleteHard(uuid_p);
        SviluppatoriUtils.deleteHard(uuid_s);
        ClientiUtils.deleteHard(uuid_c);

        AttivitaUtils.notExist(uuid_a);
        ProgettiUtils.notExist(uuid_p);
        SviluppatoriUtils.notExist(uuid_s);
    }
}
