package it.ictgroup.btest1.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Table(name = Cliente.TABLE_NAME)
@Entity
public class Cliente implements Serializable {
    public static final String TABLE_NAME = "clienti";
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "uuid", unique = true)
    @Id
    public String uuid;
    public String nome;
    public boolean active = true;

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<Progetto> progetti = new ArrayList<>();

    public Cliente() {
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "uuid='" + uuid + '\'' +
                ", nome=" + nome + '\'' +
                ", progetti=" + progetti + '\'' +
                ", active=" + active + '\'' +
                '}';
    }

}
