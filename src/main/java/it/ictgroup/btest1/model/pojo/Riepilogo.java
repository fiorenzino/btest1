package it.ictgroup.btest1.model.pojo;

import java.util.ArrayList;
import java.util.List;

public class Riepilogo {
    public String data_dal;
    public String data_al;
    public String codice_progetto;
    public String codice_cliente;
    public int ore;
    public int giorni;
    public List<RiepilogoSviluppatore> sviluppatori;

    public Riepilogo() {
    }

    public Riepilogo add(RiepilogoSviluppatore sviluppatore) {
        if (sviluppatori == null) {
            this.sviluppatori = new ArrayList<>();
        }
        this.sviluppatori.add(sviluppatore);
        this.giorni += sviluppatore.giorni;
        this.ore += sviluppatore.ore;
        return this;
    }
}
