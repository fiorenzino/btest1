package it.ictgroup.btest1.model.pojo;

import it.ictgroup.btest1.model.Sviluppatore;

public class RiepilogoSviluppatore {
    public Sviluppatore sviluppatore;
    public int ore;
    public int giorni;

    public RiepilogoSviluppatore() {
    }
}
