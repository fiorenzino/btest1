package it.ictgroup.btest1.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static it.ictgroup.btest1.model.Progetto.TABLE_NAME;


@Table(name = TABLE_NAME)
@Entity
public class Progetto implements Serializable {
    public static final String TABLE_NAME = "progetti";

    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "uuid", unique = true)
    @Id
    public String uuid;
    public String nome;
    public String codice;
    public boolean active = true;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "responsabile")
    public Sviluppatore sviluppatore;

    @Column(insertable = false, updatable = false)
    public String responsabile;


    @OneToMany(mappedBy = "progetto", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<Autorizzazione> autorizzazioni = new ArrayList<>();

    @Column(insertable = false, updatable = false)
    public String uuid_cliente;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "uuid_cliente")
    public Cliente cliente;

    public Progetto addAutorizzazione(Autorizzazione autorizzazione) {
        this.autorizzazioni.add(autorizzazione);
        return this;
    }

    public Progetto() {
    }


    @Override
    public String toString() {
        return "Progetto{" +
                "uuid='" + uuid + '\'' +
                ", nome=" + nome + '\'' +
                ", codice=" + codice + '\'' +
                ", responsabile=" + responsabile + '\'' +
                ", sviluppatore=" + sviluppatore + '\'' +
                ", autorizzazioni=" + autorizzazioni + '\'' +
                ", uuid_cliente=" + uuid_cliente + '\'' +
                ", active=" + active + '\'' +
                '}';
    }

}
