package it.ictgroup.btest1.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

import static it.ictgroup.btest1.model.Autorizzazione.TABLE_NAME;

@Table(name = TABLE_NAME)
@Entity
public class Autorizzazione implements Serializable {

    public static final String TABLE_NAME = "autorizzazioni";

    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "uuid", unique = true)
    @Id
    public String uuid;
    @Temporal(TemporalType.DATE)
    public Date data_inizio;
    @Temporal(TemporalType.DATE)
    public Date data_fine;
    public boolean active = true;


    @Column(insertable = false, updatable = false)
    public String uuid_progetto;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "uuid_progetto")
    public Progetto progetto;

    @Column(insertable = false, updatable = false)
    public String uuid_sviluppatore;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "uuid_sviluppatore")
    public Sviluppatore sviluppatore;

    public Autorizzazione() {
    }

    @Override
    public String toString() {
        return "Autorizzazione{" +
                "uuid='" + uuid + '\'' +
                ", data_inizio=" + data_inizio + '\'' +
                ", data_fine=" + data_fine + '\'' +
                ", uuid_sviluppatore=" + uuid_sviluppatore + '\'' +
                ", uuid_progetto=" + uuid_progetto + '\'' +
                ", active=" + active + '\'' +
                '}';
    }
}
