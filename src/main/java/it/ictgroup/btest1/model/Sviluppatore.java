package it.ictgroup.btest1.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static it.ictgroup.btest1.model.Sviluppatore.TABLE_NAME;

@Table(name = TABLE_NAME)
@Entity
public class Sviluppatore implements Serializable {
    public static final String TABLE_NAME = "sviluppatori";

    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "uuid", unique = true)
    @Id
    public String uuid;
    public String nome;
    public String cognome;
    public String email;
    public boolean active = true;

    @OneToMany(mappedBy = "sviluppatore", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<Progetto> progetti = new ArrayList<>();

    @OneToMany(mappedBy = "sviluppatore", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<Autorizzazione> autorizzazioni = new ArrayList<>();

    public Sviluppatore() {
    }

    @Override
    public String toString() {
        return "Sviluppatore{" +
                "uuid='" + uuid + '\'' +
                ", nome=" + nome + '\'' +
                ", cognome=" + cognome + '\'' +
                ", email=" + email + '\'' +
                ", progetti=" + progetti + '\'' +
                ", autorizzazioni=" + autorizzazioni + '\'' +
                ", active=" + active + '\'' +
                '}';
    }
}
