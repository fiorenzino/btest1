package it.ictgroup.btest1.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Date;


import static it.ictgroup.btest1.model.Attivita.TABLE_NAME;

@Table(name = TABLE_NAME)
@Entity
public class Attivita implements Serializable {
    public static final String TABLE_NAME = "attivita";

    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "uuid", unique = true)
    @Id
    public String uuid;

    @Temporal(TemporalType.DATE)
    public Date data;
    public int ore;
    public String descrizione;

    //AGGANCIO CON PROGETTTO
    public String codice_progetto;
    //AGGANCIO CON SVILUPPATORE
    public String email_sviluppatore;
    public String citta;
    public String uuid_uploadoperation;

    //PROVO A TENERE TRACCIA DEL CREATORE
    public String operatore;

    public boolean active = true;


    public Attivita() {
    }

    @Override
    public String toString() {
        return "Attivita{" +
                "uuid='" + uuid + '\'' +
                ", data=" + data + '\'' +
                ", ore=" + ore + '\'' +
                ", descrizione=" + descrizione + '\'' +
                ", codice_progetto=" + codice_progetto + '\'' +
                ", email_sviluppatore=" + email_sviluppatore + '\'' +
                ", uuid_uploadoperation=" + uuid_uploadoperation + '\'' +
                ", citta=" + citta + '\'' +
                ", operatore=" + operatore + '\'' +
                ", active=" + active + '\'' +
                '}';
    }
}
