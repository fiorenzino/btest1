package it.ictgroup.btest1.model.enums;

public enum MassiveType {
    MASSIVE_SYNC, ASYNC, QUEUE;
}
