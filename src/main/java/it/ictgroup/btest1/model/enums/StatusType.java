package it.ictgroup.btest1.model.enums;

public enum StatusType {
    ATTIVO, ERRORE, COMPLETATO
}
