package it.ictgroup.btest1.model;

import it.ictgroup.btest1.model.enums.StatusType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static it.ictgroup.btest1.model.UploadOperation.TABLE_NAME;

@Table(name = TABLE_NAME)
@Entity
public class UploadOperation implements Serializable {

    public static final String TABLE_NAME = "uploadOperation";
    @Id
    public String uuid;
    @Temporal(TemporalType.DATE)
    public Date data_creazione;
    @Temporal(TemporalType.DATE)
    public Date data_fine_lavorazione;
    public long num_righe;
    public String nome_file;
    @Enumerated(EnumType.STRING)
    public StatusType statusType;


    public UploadOperation(){

    }

    @Override
    public String toString() {
        return "Attivita{" +
                "uuid='" + uuid + '\'' +
                ", data_creazione=" + data_creazione + '\'' +
                ", data_fine_lavorazione=" + data_fine_lavorazione + '\'' +
                ", num_righe=" + num_righe + '\'' +
                ", nome_file=" + nome_file + '\'' +
                ", statusType=" + statusType + '\'' +
                '}';
    }
}
