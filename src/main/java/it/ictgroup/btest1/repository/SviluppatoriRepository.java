package it.ictgroup.btest1.repository;

import it.ictgroup.btest1.model.Attivita;
import it.ictgroup.btest1.model.Sviluppatore;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
@LocalBean
public class SviluppatoriRepository extends BaseRepository<Sviluppatore> {
    @Override
    public List<Sviluppatore> getList(int startRow, int pageSize) throws Exception {
        Query query = getEm().createQuery("SELECT p FROM " + Sviluppatore.class.getName() + " p");
        if (startRow >= 0) {
            query.setFirstResult(startRow);
        }
        if (pageSize > 0) {
            query.setMaxResults(pageSize);
        }
        List<Sviluppatore> sviluppatori = query.getResultList();
        return sviluppatori;
    }

    @Override
    public int getListSize() throws Exception {
        int numraws = getEm().createNativeQuery("SELECT COUNT(uuid) FROM " + Sviluppatore.TABLE_NAME)
                .getMaxResults();
        return numraws;
    }

    @Override
    public Sviluppatore find(Object key) throws Exception {
        Sviluppatore sviluppatore = getEm().find(Sviluppatore.class, key);
        return sviluppatore;
    }

    @Override
    public Sviluppatore fetch(Object key) throws Exception {
        getEm().createQuery("SELECT s FROM " + Sviluppatore.class.getName() + " s LEFT JOIN FETCH s.progetti where s.uuid =:key")
                .setParameter("key", key);
        return find(key);
    }

    @Override
    public Sviluppatore persist(Sviluppatore object) throws Exception {
        getEm().persist(object);
        return object;
    }

    @Override
    public Sviluppatore update(Sviluppatore object) throws Exception {
        getEm().merge(object);
        return object;
    }

    @Override
    public void delete(Object key) throws Exception {
        getEm().createNativeQuery("update " + Sviluppatore.TABLE_NAME + " set active = :active where uuid = :key ")
                .setParameter("active", false).setParameter("key", key).executeUpdate();
    }

    public void hard_delete(Object key) throws Exception {
        Sviluppatore obj = getEm().find(Sviluppatore.class, key);
        getEm().remove(obj);
    }

    public boolean verificaPerEmail(String email) throws Exception {
        Long result = (Long) getEm().createQuery("SELECT COUNT(p) FROM " + Sviluppatore.class.getName() + " p where p.email = :EMAIL ")
                .setParameter("EMAIL", email).getSingleResult();
        return result > 0;
    }

    public boolean verificaPerEmailUpdate(String email, String uuid) throws Exception {
        Long result = (Long) getEm().createQuery("SELECT COUNT(p) FROM " + Sviluppatore.class.getName() + " p where p.email = :EMAIL and p.uuid IS NOT :UUID")
                .setParameter("EMAIL", email).setParameter("UUID", uuid).getSingleResult();
        return result > 0;
    }

    public Sviluppatore findByEmail(String email) throws Exception {
        List<Sviluppatore> sviluppatori = getEm().createQuery("SELECT p FROM " + Sviluppatore.class.getName() + " p where p.email = :EMAIL")
                .setParameter("EMAIL", email)
                .setMaxResults(1)
                .getResultList();
        return sviluppatori.isEmpty() ? null : sviluppatori.get(0);
    }

    public List<Sviluppatore> getAllSviluppatori() {
        return  (List<Sviluppatore>) getEm().createQuery("SELECT p FROM " + Sviluppatore.class.getName() + " p").getResultList();
    }

    public List<Attivita> getAttivitaByUploadOperationUUid(String uuid) {
        List<Attivita> attivitas = (List<Attivita>) em.createNativeQuery("SELECT COUNT(uuid) FROM " + Attivita.TABLE_NAME + " where uuid_uploadoperation = :UUID")
                .setParameter("UUID", uuid)
                .getResultList();
        return attivitas;
    }

    public Map<String,String> getEmailAndSurname(List<String> emails) throws Exception {
        Map<String, String> sviluppatore = new HashMap<>();
        for (String email : emails){
            sviluppatore.put(email, findByEmail(email).cognome);
        }
        return sviluppatore;
    }
}
