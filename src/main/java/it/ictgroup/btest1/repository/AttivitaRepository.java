package it.ictgroup.btest1.repository;

import it.ictgroup.btest1.model.Attivita;
import it.ictgroup.btest1.model.Progetto;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Stateless
@LocalBean
public class AttivitaRepository extends BaseRepository<Attivita> {
    @Override
    public List<Attivita> getList(int startRow, int pageSize) throws Exception {
        Query query = getEm().createQuery("SELECT p FROM " + Attivita.class.getName() + " p where p.active=true");
        if (startRow >= 0) {
            query.setFirstResult(startRow);
        }
        if (pageSize > 0) {
            query.setMaxResults(pageSize);
        }
        List<Attivita> sviluppatori = query.getResultList();
        return sviluppatori;
    }

    @Override
    public int getListSize() throws Exception {
        int numraws = getEm().createNativeQuery("SELECT COUNT(uuid) FROM " + Attivita.TABLE_NAME)
                .getMaxResults();
        return numraws;
    }

    @Override
    public Attivita find(Object key) throws Exception {
        Attivita attivita = getEm().find(Attivita.class, key);
        return attivita;
    }

    @Override
    public Attivita fetch(Object key) throws Exception {
        return null;
    }

    @Override
    public Attivita persist(Attivita object) throws Exception {
        getEm().persist(object);
        return object;
    }

    @Override
    public Attivita update(Attivita object) throws Exception {
        getEm().merge(object);
        return object;
    }

    @Override
    public void delete(Object key) throws Exception {
        getEm().createNativeQuery("update " + Attivita.TABLE_NAME + " set active = :active where uuid = :key ")
                .setParameter("active", false).setParameter("key", key).executeUpdate();
    }

    public void hard_delete(Object key) throws Exception {
        Attivita obj = getEm().find(Attivita.class, key);
        getEm().remove(obj);
    }

    public int verificaOreSviluppatore(Attivita attivita) throws Exception {
        Query res = getEm().createNativeQuery("SELECT SUM(ore) FROM  " + Attivita.TABLE_NAME +
                " where  email_sviluppatore = :MAIL and date(attivita.data) = :DATA")
                .setParameter("MAIL", attivita.email_sviluppatore)
                .setParameter("DATA", attivita.data, TemporalType.DATE);
        Number result = (Number) res.getSingleResult();
        if (result == null) {
            result = new BigDecimal(0);
        }

        return result.intValue();
    }

    public List<Attivita> getAttivitaByUploadOperationUUid(String uuid){
        List<Attivita> obj = getEm().createNativeQuery("SELECT * FROM  " + Attivita.TABLE_NAME +
                " where  uuid_uploadoperation = :UUID")
                .setParameter("UUID", uuid).getResultList();
        return obj;
    }

    public List<String> emails(String data_dal,
                               String data_al,
                               String email_sviluppatore,
                               String codice_progetto,
                               String codice_cliente) throws Exception {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String alias = "c";
        StringBuffer sb = new StringBuffer("select distinct( " + alias + ".email_sviluppatore) from " + Attivita.class.getSimpleName() + " "
                + alias + " ");
        Map<String, Object> params = new HashMap<>();
        String separator = " where ";

        if (email_sviluppatore != null && !email_sviluppatore.trim().isEmpty()) {
            return Collections.singletonList(email_sviluppatore);
        }

        if (data_dal != null && !data_dal.trim().isEmpty()) {
            Date date = format.parse(data_dal);
            sb.append(separator).append(alias).append(".data >= :DATA_DAL ");
            params.put("DATA_DAL", date);
            separator = " and ";
        }

        if (data_al != null && !data_al.trim().isEmpty()) {
            Date date = format.parse(data_al);
            sb.append(separator).append(alias).append(".data <= :DATA_AL ");
            params.put("DATA_AL", date);
            separator = " and ";
        }

        if (codice_progetto != null && !codice_progetto.trim().isEmpty()) {
            sb.append(separator).append(alias).append(".codice_progetto = :PROGETTO ");
            params.put("PROGETTO", codice_progetto);
            separator = " and ";
        }

        if (codice_cliente != null && !codice_cliente.trim().isEmpty()) {
            sb.append(separator).append(alias).append(".codice_progetto = (SELECT distinct(p.codice) From " + Progetto.class.getSimpleName() + " p where p.uuid_cliente = :CLIENTE and p.codice = :PROGETTO) ");
            params.put("CLIENTE", codice_cliente);
            params.put("PROGETTO", codice_progetto);
        }

        sb.append("ORDER BY ").append(alias).append(".email_sviluppatore ASC");
        Query res = getEm().createQuery(sb.toString());

        for (String param : params.keySet()) {
            res.setParameter(param, params.get(param));
        }
        List<String> result = (List<String>) res.getResultList();

        return result;
    }

    public List<String> codici(String data_dal,
                               String data_al,
                               String email_sviluppatore,
                               String codice_progetto,
                               String codice_cliente) throws Exception {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String alias = "c";
        StringBuffer sb = new StringBuffer("select distinct( " + alias + ".codice_progetto) from " + Attivita.class.getSimpleName() + " "
                + alias + " ");
        Map<String, Object> params = new HashMap<>();
        String separator = " where ";

        if (codice_progetto != null && !codice_progetto.trim().isEmpty()) {
            return Collections.singletonList(codice_progetto);
        }

        if (data_dal != null && !data_dal.trim().isEmpty()) {
            Date date = format.parse(data_dal);
            sb.append(separator).append(alias).append(".data >= :DATA_DAL ");
            params.put("DATA_DAL", date);
            separator = " and ";
        }

        if (data_al != null && !data_al.trim().isEmpty()) {
            Date date = format.parse(data_al);
            sb.append(separator).append(alias).append(".data <= :DATA_AL ");
            params.put("DATA_AL", date);
            separator = " and ";
        }

        if (email_sviluppatore != null && !email_sviluppatore.trim().isEmpty()) {
            sb.append(separator).append(alias).append(".email_sviluppatore = :EMAIL ");
            params.put("EMAIL", email_sviluppatore);
            separator = " and ";
        }

        if (codice_cliente != null && !codice_cliente.trim().isEmpty()) {
            sb.append(separator).append(alias).append(".codice_progetto = (SELECT distinct(p.codice) From " + Progetto.class.getSimpleName() + " p where p.uuid_cliente = :CLIENTE and p.codice = :PROGETTO) ");
            params.put("CLIENTE", codice_cliente);
            params.put("PROGETTO", codice_progetto);
        }

        Query res = getEm().createQuery(sb.toString());

        for (String param : params.keySet()) {
            res.setParameter(param, params.get(param));
        }
        List<String> result = res.getResultList();

        return result;
    }

    public List<Attivita> report(String data_dal,
                                        String data_al,
                                        String email_sviluppatore,
                                        String codice_progetto,
                                        String codice_cliente) throws Exception {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String alias = "c";
        StringBuffer sb = new StringBuffer("select " + alias + " from " + Attivita.class.getSimpleName() + " "
                + alias + " ");
        Map<String, Object> params = new HashMap<>();
        String separator = " where ";

        //LO FACCsIO PER TUTTI I PARAMETRI

        if (data_dal != null && !data_dal.trim().isEmpty()) {
            Date date = format.parse(data_dal);
            sb.append(separator).append(alias).append(".data >= :DATA_DAL ");
            params.put("DATA_DAL", date);
            separator = " and ";
        }

        if (data_al != null && !data_al.trim().isEmpty()) {
            Date date = format.parse(data_al);
            sb.append(separator).append(alias).append(".data <= :DATA_AL ");
            params.put("DATA_AL", date);
            separator = " and ";
        }

        if (email_sviluppatore != null && !email_sviluppatore.trim().isEmpty()) {
            sb.append(separator).append(alias).append(".email_sviluppatore = :EMAIL ");
            params.put("EMAIL", email_sviluppatore);
            separator = " and ";
        }

        if (codice_progetto != null && !codice_progetto.trim().isEmpty()) {
            sb.append(separator).append(alias).append(".codice_progetto = :PROGETTO ");
            params.put("PROGETTO", codice_progetto);
            separator = " and ";
        }

        if (codice_cliente != null && !codice_cliente.trim().isEmpty()) {
            sb.append(separator).append(alias).append(".codice_progetto = (SELECT distinct(p.codice) From " + Progetto.class.getSimpleName() + " p where p.uuid_cliente = :CLIENTE and p.codice = :PROGETTO) ");
            params.put("CLIENTE", codice_cliente);
            params.put("PROGETTO", codice_progetto);
        }

        sb.append("ORDER BY ").append(alias).append(".data ASC, ").append(alias).append(".email_sviluppatore ASC");
        Query res = getEm().createQuery(sb.toString());

        for (String param : params.keySet()) {
            res.setParameter(param, params.get(param));
        }
        List<Attivita> result = (List<Attivita>) res.getResultList();

        return result;
    }

    public int working_hours(String data_dal,
                             String data_al,
                             String email_sviluppatore,
                             String codice_progetto,
                             String codice_cliente) throws Exception {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String alias = "c";
        StringBuffer sb = new StringBuffer("select sum(" + alias + ".ore) from " + Attivita.class.getSimpleName() + " "
                + alias + " ");
        Map<String, Object> params = new HashMap<>();
        String separator = " where ";

        //LO FACCsIO PER TUTTI I PARAMETRI

        if (data_dal != null && !data_dal.trim().isEmpty()) {
            Date date = format.parse(data_dal);
            sb.append(separator).append(alias).append(".data >= :DATA_DAL ");
            params.put("DATA_DAL", date);
            separator = " and ";
        }

        if (data_al != null && !data_al.trim().isEmpty()) {
            Date date = format.parse(data_al);
            sb.append(separator).append(alias).append(".data <= :DATA_AL ");
            params.put("DATA_AL", date);
            separator = " and ";
        }

        if (email_sviluppatore != null && !email_sviluppatore.trim().isEmpty()) {
            sb.append(separator).append(alias).append(".email_sviluppatore = :EMAIL ");
            params.put("EMAIL", email_sviluppatore);
            separator = " and ";
        }

        if (codice_progetto != null && !codice_progetto.trim().isEmpty()) {
            sb.append(separator).append(alias).append(".codice_progetto = :PROGETTO ");
            params.put("PROGETTO", codice_progetto);
            separator = " and ";
        }

        if (codice_cliente != null && !codice_cliente.trim().isEmpty()) {
            sb.append(separator).append(alias).append(".codice_progetto = (SELECT distinct(p.codice) from " + Progetto.class.getSimpleName() + " p where p.uuid_cliente = :CLIENTE and p.codice = :PROGETTO) ");
            params.put("CLIENTE", codice_cliente);
            params.put("PROGETTO", codice_progetto);
        }

        Query res = getEm().createQuery(sb.toString());

        for (String param : params.keySet()) {
            res.setParameter(param, params.get(param));
        }
        Number result = (Number) res.getSingleResult();
        if (result == null)
            return 0;
        return result.intValue();
    }
}
