package it.ictgroup.btest1.repository;

import it.ictgroup.btest1.model.UploadOperation;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

@Stateless
@LocalBean
public class UploadOperationRepository extends BaseRepository<UploadOperation> {
    @Override
    public List<UploadOperation> getList(int startRow, int pageSize) throws Exception {
        Query query = getEm().createQuery("SELECT p FROM " + UploadOperation.class.getName() + " p");
        if (startRow >= 0) {
            query.setFirstResult(startRow);
        }
        if (pageSize > 0) {
            query.setMaxResults(pageSize);
        }
        List<UploadOperation> uploadOperations = query.getResultList();
        return uploadOperations;
    }

    @Override
    public int getListSize() throws Exception {
        int numraws = getEm().createNativeQuery("SELECT COUNT(uuid) FROM " + UploadOperation.TABLE_NAME)
                .getMaxResults();
        return numraws;
    }

    @Override
    public UploadOperation find(Object key) throws Exception {
        UploadOperation uploadOperation = getEm().find(UploadOperation.class, key);
        return uploadOperation;
    }

    @Override
    public UploadOperation fetch(Object key) throws Exception {
        getEm().createQuery("SELECT s FROM " + UploadOperation.class.getName() + " s LEFT JOIN FETCH s.progetti where s.uuid =:key")
                .setParameter("key", key);
        return find(key);
    }

    @Override
    public UploadOperation persist(UploadOperation object) throws Exception {
        getEm().persist(object);
        return object;
    }

    @Override
    public UploadOperation update(UploadOperation object) throws Exception {
        getEm().merge(object);
        return object;
    }

    @Override
    public void delete(Object key) throws Exception {
        UploadOperation obj = getEm().find(UploadOperation.class, key);
        getEm().remove(obj);
    }

    public List<UploadOperation> getUploadOperationsWithEmptyEndDate() {
        List<UploadOperation> uploadOperations = (List<UploadOperation>) em.createNativeQuery("select " + UploadOperation.TABLE_NAME + "  where data_fine_lavorazione = :key ")
                .setParameter("key", null);
        return uploadOperations;
    }
}
