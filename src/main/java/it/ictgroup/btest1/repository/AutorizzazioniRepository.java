package it.ictgroup.btest1.repository;

import it.ictgroup.btest1.model.Attivita;
import it.ictgroup.btest1.model.Autorizzazione;
import it.ictgroup.btest1.model.Progetto;
import it.ictgroup.btest1.model.Sviluppatore;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class AutorizzazioniRepository extends BaseRepository<Autorizzazione> {
    @Override
    public List<Autorizzazione> getList(int startRow, int pageSize) throws Exception {
        Query query = getEm().createQuery("SELECT p FROM " + Autorizzazione.class.getName() + " p");
        if (startRow >= 0) {
            query.setFirstResult(startRow);
        }
        if (pageSize > 0) {
            query.setMaxResults(pageSize);
        }
        List<Autorizzazione> autorizzazioni = query.getResultList();
        return autorizzazioni;
    }

    @Override
    public int getListSize() throws Exception {
        int numraws = getEm().createNativeQuery("SELECT COUNT(uuid) FROM " + Autorizzazione.TABLE_NAME)
                .getMaxResults();
        return numraws;
    }

    @Override
    public Autorizzazione find(Object key) throws Exception {
        Autorizzazione autorizzazioni = getEm().find(Autorizzazione.class, key);
        return autorizzazioni;
    }

    @Override
    public Autorizzazione fetch(Object key) throws Exception {
        return null;
    }

    @Override
    public Autorizzazione persist(Autorizzazione object) throws Exception {
        getEm().persist(object);
        return object;
    }

    @Override
    public Autorizzazione update(Autorizzazione object) throws Exception {
        getEm().merge(object);
        return object;
    }

    @Override
    public void delete(Object key) throws Exception {
        getEm().createNativeQuery("update " + Autorizzazione.TABLE_NAME + " set active = :active where uuid = :key ")
                .setParameter("active", false).setParameter("key", key).executeUpdate();
    }


    public boolean esisteAutorizzazione(String uuid_progetto, String uuid_sviluppatore, Date data) throws Exception {

        Query res = getEm().createNativeQuery(
                "select count(*) from " + Autorizzazione.TABLE_NAME +
                        " where uuid_progetto = :UUID_PROGETTO " +
                        " and uuid_sviluppatore = :SVILUPPATORE " +
                        " and active = :ACTIVE " +
                        " and data_inizio <= :DATA_INIZIO" +
                        " and data_fine >= :DATA_FINE")
                .setParameter("UUID_PROGETTO", uuid_progetto)
                .setParameter("SVILUPPATORE", uuid_sviluppatore)
                .setParameter("ACTIVE", true)
                .setParameter("DATA_INIZIO", data)
                .setParameter("DATA_FINE", data);
        int result = ((Number) res.getSingleResult()).intValue();
        return result > 0;
    }

//    public Autorizzazione create(Progetto progetto, Sviluppatore sviluppatore){
//        return getEm().createNativeQuery("insert into " + Autorizzazione.TABLE_NAME + " set uuid`, `active`, `data_fine`, `data_inizio`, `uuid_progetto`, `uuid_sviluppatore`) VALUES ('123', true, '2018-01-01', '2019-12-31', '1', '1'); ")
//                .setParameter("active", false).setParameter("key", key).executeUpdate();
//    }

}
