package it.ictgroup.btest1.repository;


import it.ictgroup.btest1.common.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class BaseRepository<T> implements Repository<T> {

    private static final long serialVersionUID = 1L;

    @PersistenceContext
    EntityManager em;

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

}
