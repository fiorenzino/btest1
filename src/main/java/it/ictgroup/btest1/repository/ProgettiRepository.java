package it.ictgroup.btest1.repository;

import it.ictgroup.btest1.model.Progetto;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
@LocalBean
public class ProgettiRepository extends BaseRepository<Progetto> {
    @Override
    public List<Progetto> getList(int startRow, int pageSize) throws Exception {
        Query query = getEm().createQuery("SELECT p FROM " + Progetto.class.getName() + " p");
        if (startRow >= 0) {
            query.setFirstResult(startRow);
        }
        if (pageSize > 0) {
            query.setMaxResults(pageSize);
        }
        List<Progetto> progetti = query.getResultList();
        return progetti;
    }

    @Override
    public int getListSize() throws Exception {
        Query res = getEm().createQuery("SELECT count(p) FROM " + Progetto.class.getName() + " p");
        return ((Long) res.getSingleResult()).intValue();
    }

    @Override
    public Progetto find(Object key) throws Exception {
        Progetto progetto = getEm().find(Progetto.class, key);
        return progetto;
    }

    @Override
    public Progetto fetch(Object key) throws Exception {
        getEm().createQuery("SELECT p FROM " + Progetto.class.getName() + " p LEFT JOIN FETCH p.attivitas where p.uuid =:key")
                .setParameter("key", key);

        return find(key);
    }

    @Override
    public Progetto persist(Progetto object) throws Exception {
        getEm().persist(object);
        return object;
    }

    @Override
    public Progetto update(Progetto object) throws Exception {
        getEm().merge(object);
        return object;
    }

    @Override
    public void delete(Object key) throws Exception {
        getEm().createNativeQuery("update " + Progetto.TABLE_NAME + " set active = :active where uuid = :key ")
                .setParameter("active", false).setParameter("key", key).executeUpdate();
    }

    public void delete_hard(Object key) throws Exception {
        Progetto obj = getEm().find(Progetto.class, key);
        getEm().remove(obj);
    }

    public int findByNomeCodice(String nome, String codice) throws Exception {
        Query res = getEm().createQuery("SELECT COUNT(p) FROM " + Progetto.class.getName() + " p where p.codice = :CODICE or p.nome =:NOME")
                .setParameter("CODICE", codice).setParameter("NOME", nome);
        return ((Long) res.getSingleResult()).intValue();
    }

    //DA VERIFICARE
    public Progetto findByCodice(String codice) throws Exception {
        List<Progetto> progetti = getEm()
                .createQuery("select p from " + Progetto.class.getName() + " p where p.codice = :CODICE").setParameter("CODICE", codice)
                .setMaxResults(1)
                .getResultList();
        return progetti.isEmpty() ? null : progetti.get(0);
    }

    public List<Progetto> findCodice(String codice) throws Exception {
        List<Progetto> progetti = getEm()
                .createQuery("select p from " + Progetto.class.getName() + " p where p.codice like :CODICE").setParameter("CODICE", codice +"%")
                .getResultList();
        return progetti.isEmpty() ? null : progetti;
    }

    public List<Progetto> getAllProgetti() {
        return  (List<Progetto>) getEm().createQuery("SELECT p FROM " + Progetto.class.getName() + " p").getResultList();
    }

    public Map<String,String> getCodeAndName(List<String> codici) throws Exception {
        Map<String, String> progetto = new HashMap<>();
        for (String codice : codici) {
            progetto.put(codice, findByCodice(codice).nome);
        }
        return progetto;
    }
}
