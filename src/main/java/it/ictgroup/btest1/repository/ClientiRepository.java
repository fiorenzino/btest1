package it.ictgroup.btest1.repository;

import io.undertow.client.ClientStatistics;
import it.ictgroup.btest1.model.Cliente;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;
@Stateless
@LocalBean
public class ClientiRepository extends BaseRepository<Cliente> {
    @Override
    public List<Cliente> getList(int startRow, int pageSize) throws Exception {
        Query query = getEm().createQuery("SELECT p FROM " + Cliente.class.getName() + " p");
        if (startRow >= 0) {
            query.setFirstResult(startRow);
        }
        if (pageSize > 0) {
            query.setMaxResults(pageSize);
        }
        List<Cliente> clienti = query.getResultList();
        return clienti;
    }

    @Override
    public int getListSize() throws Exception {
        int numraws = getEm().createNativeQuery("SELECT COUNT(uuid) FROM " + Cliente.TABLE_NAME)
                .getMaxResults();
        return numraws;
    }

    @Override
    public Cliente find(Object key) throws Exception {
        Cliente cliente = getEm().find(Cliente.class, key);
        return cliente;
    }

    @Override
    public Cliente fetch(Object key) throws Exception {
        getEm().createQuery("SELECT s FROM " + Cliente.class.getName() + " s LEFT JOIN FETCH s.progetti where s.uuid =:key")
                .setParameter("key", key);
        return find(key);
    }

    @Override
    public Cliente persist(Cliente object) throws Exception {
        getEm().persist(object);
        return object;
    }

    @Override
    public Cliente update(Cliente object) throws Exception {
        getEm().merge(object);
        return object;
    }

    @Override
    public void delete(Object key) throws Exception {
        getEm().createNativeQuery("update " + Cliente.TABLE_NAME + " set active = :active where uuid = :key ")
                .setParameter("active", false).setParameter("key", key).executeUpdate();
    }

    public void hard_delete(Object key) throws Exception {
        Cliente obj = getEm().find(Cliente.class, key);
        getEm().remove(obj);
    }

    public Cliente findByNome(String nome) throws Exception {
        List<Cliente> sviluppatori = getEm().createQuery("SELECT p FROM " + Cliente.class.getName() + " p where p.nome = :NOME")
                .setParameter("NOME", nome)
                .setMaxResults(1)
                .getResultList();
        return sviluppatori.isEmpty() ? null : sviluppatori.get(0);
    }


}
