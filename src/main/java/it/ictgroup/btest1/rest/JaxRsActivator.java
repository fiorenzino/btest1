package it.ictgroup.btest1.rest;


import it.ictgroup.btest1.management.AppConstants;

import javax.ws.rs.ApplicationPath;

@ApplicationPath(AppConstants.API_PATH)
public class JaxRsActivator extends javax.ws.rs.core.Application {

}
