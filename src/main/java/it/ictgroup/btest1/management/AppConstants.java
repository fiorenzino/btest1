package it.ictgroup.btest1.management;

public class AppConstants {

    public static final String APP_NAME = "pilomaxx";

    public static final String API_PATH = "/api";

    public static final String ATTIVITA_PATH = "/v1/attivita";
    public static final String AUTORIZZAZIONI_PATH = "v1/autorizzazioni";
    public static final String CLIENTI_PATH = "/v1/clienti";
    public static final String ESTRAZIONI_PATH = "/v1/estrazioni";
    public static final String PROGETTI_PATH = "/v1/progetti";
    public static final String SVILUPPATORI_PATH = "/v1/sviluppatori";
    public static final String UPLOAD_OPERATIONS_PATH = "/v1/uploadoperations";


    public static final String ATTIVITA_QUEUE = "attivita-queue";


    public static final String MSG_LINE = "line";
    public static final String MSG_OPERATORE = "operatore";
    public static final String MSG_UUID = "uuid";
    public static final String SEPARATOR_SYMBOL = ";";


    public static final String csvUploadPath = "/tmp/";


    public AppConstants() {
    }
}
