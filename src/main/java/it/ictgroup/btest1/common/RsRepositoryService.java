package it.ictgroup.btest1.common;

import org.jboss.logging.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.Serializable;

public abstract class RsRepositoryService<T> implements Serializable {

    private static final long serialVersionUID = 1L;
    protected static final Logger logger = Logger.getLogger(RsRepositoryService.class);

    protected Repository<T> repository;

    public RsRepositoryService() {
    }

    public RsRepositoryService(Repository<T> repository) {
        this.repository = repository;
    }


    public abstract Response persist(T object) throws Exception;

    public abstract Response fetch(String id);

    public abstract Response update(String id, T object) throws Exception;

    public abstract Response delete(String id) throws Exception;

    public abstract Response getList(
            @DefaultValue("0") @QueryParam("startRow") Integer startRow,
            @DefaultValue("10") @QueryParam("pageSize") Integer pageSize,
            @QueryParam("orderBy") String orderBy, @Context UriInfo ui) throws Exception;

    public static Response jsonResponse(Response.Status status) {
        return Response.status(status).build();
    }

    public static Response jsonResponse(Response.Status status, Object value) {
        return Response.status(status).entity(value).build();
    }

    public static Response jsonResponse(Response.Status status, String value) {
        JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add("msg", value);
        JsonObject jsonObj = jsonObjBuilder.build();
        return Response.status(status).entity(jsonObj.toString()).build();
    }

    public static Response jsonResponse(Response.Status status, String key, Object value) {
        JsonObjectBuilder jsonObjBuilder = Json.createObjectBuilder();
        jsonObjBuilder.add(key, value.toString());
        JsonObject jsonObj = jsonObjBuilder.build();
        return Response.status(status).entity(jsonObj.toString()).build();
    }

    protected Repository<T> getRepository() {
        return repository;
    }
}
