package it.ictgroup.btest1.common;

import java.util.List;

/**
 * @param <T>
 */
public interface Repository<T> {


    public List<T> getList(int startRow, int pageSize)
            throws Exception;


    public int getListSize() throws Exception;


    public T find(Object key) throws Exception;


    public T fetch(Object key) throws Exception;


    public T persist(T object) throws Exception;


    public T update(T object) throws Exception;


    public void delete(Object key) throws Exception;


}
