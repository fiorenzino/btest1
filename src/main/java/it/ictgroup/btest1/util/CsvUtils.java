package it.ictgroup.btest1.util;

import it.ictgroup.btest1.model.Attivita;
import it.ictgroup.btest1.model.Progetto;
import it.ictgroup.btest1.model.Sviluppatore;
import it.ictgroup.btest1.repository.ProgettiRepository;
import it.ictgroup.btest1.repository.SviluppatoriRepository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by fiorenzo on 22/06/16.
 */
public class CsvUtils {

    static String INIT_END = "\"";
    static String DIVIDER = "\";\"";
    static String END_LINE = "\";\n";


    public static String fromAttivita(List<Attivita> list, Map<String, String> progetto, Map<String, String> sviluppatore) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        StringBuffer writer = new StringBuffer();
        writer.append(INIT_END).append("data").append(DIVIDER)
                .append("ore").append(DIVIDER)
                .append("codice_progetto").append(DIVIDER)
                .append("nome_progetto").append(DIVIDER)
                .append("cognome").append(DIVIDER)
                .append("descrizione").append(DIVIDER)
                .append("citta").append(END_LINE);
        for (Attivita attivita : list) {
            writer.append(INIT_END)
                    .append(formatDate(attivita.data, df)).append(DIVIDER)
                    .append(attivita.ore).append(DIVIDER)
                    .append(attivita.codice_progetto).append(DIVIDER)
                    .append(progetto.get(attivita.codice_progetto)).append(DIVIDER)
                    .append(sviluppatore.get(attivita.email_sviluppatore)).append(DIVIDER)
                    .append(attivita.descrizione).append(DIVIDER)
                    .append(attivita.citta).append(DIVIDER)
                    .append(END_LINE);
        }
        return writer.toString();
    }

    public static String formatDate(Date date, DateFormat dateFormat) {
        if (date != null)
            return dateFormat.format(date);
        return "";
    }
}
