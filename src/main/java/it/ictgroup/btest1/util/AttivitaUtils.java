package it.ictgroup.btest1.util;

import it.ictgroup.btest1.management.AppConstants;
import it.ictgroup.btest1.model.Attivita;
import it.ictgroup.btest1.model.Progetto;
import it.ictgroup.btest1.model.Sviluppatore;
import it.ictgroup.btest1.model.UploadOperation;
import it.ictgroup.btest1.model.pojo.MultipartFormAttivita;
import it.ictgroup.btest1.repository.AttivitaRepository;
import it.ictgroup.btest1.repository.AutorizzazioniRepository;
import it.ictgroup.btest1.repository.ProgettiRepository;
import it.ictgroup.btest1.repository.SviluppatoriRepository;
import org.jboss.logging.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AttivitaUtils {

    public static List<Attivita> fromListStringToAttivita(String operatore, List<String> lines, String uuid ) throws Exception {
        List<Attivita> attivitas = new ArrayList<>();

//        String[] fieldList = {"data", "ore", "codice_progetto", "email_sviluppatore", "citta"};

        for (String line : lines.subList( 1, lines.size() )) {
            if (line == null || line.trim().isEmpty()) {
                continue;

            }
            Attivita attivita = single(line, AppConstants.SEPARATOR_SYMBOL, uuid);
            attivita.operatore = operatore;
            if (attivita != null)
                attivitas.add(attivita);
        }
        if (attivitas.isEmpty()) {
            throw new Exception("non riesco a generare attivitas");
        }
        return attivitas;
    }


    public static Attivita single(String line, String separatorsymbol, String uuid) throws Exception {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Attivita attivita = new Attivita();
        if (line == null || line.trim().isEmpty()) {
            throw new Exception("single line is empty");
        }
        String[] split = line.split(separatorsymbol);
        if (split.length > 0) {
            attivita.citta = split[6];
            attivita.codice_progetto = split[4];
            attivita.data = format.parse(split[0]);
            attivita.descrizione = split[3] + " - " + split[5];
            attivita.email_sviluppatore = split[1];
            attivita.ore = Integer.parseInt(split[2]);
            attivita.uuid_uploadoperation = uuid;
        }
        return attivita;
    }


    public static void verifica(Attivita attivita, ProgettiRepository progettiRepository, AttivitaRepository attivitaRepository,
                                SviluppatoriRepository sviluppatoriRepository, AutorizzazioniRepository autorizzazioniRepository) throws Exception {
        if (attivita.ore > 8) {
            throw new Exception("numero ore indicate deve essere minore di 8");
        }

        Progetto progetto = progettiRepository.findByCodice(attivita.codice_progetto);
        if (progetto == null) {
            throw new Exception("Non esiste progetto");
        }
        Sviluppatore sviluppatore = sviluppatoriRepository.findByEmail(attivita.email_sviluppatore);
        if (sviluppatore == null) {
            throw new Exception("Non esiste sviluppatore");
        }

        int hours = attivitaRepository.verificaOreSviluppatore(attivita);
        if ((hours + attivita.ore) > 8) {
            throw new Exception("Per l'attività corrente hai superato il limite massimo di 8 ore");
        }

        if (!autorizzazioniRepository.esisteAutorizzazione(progetto.uuid, sviluppatore.uuid, attivita.data)) {
            throw new Exception("Non esiste un'autorizzazione ");
        }
    }


}
