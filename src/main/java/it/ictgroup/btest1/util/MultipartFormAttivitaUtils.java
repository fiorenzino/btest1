package it.ictgroup.btest1.util;

import it.ictgroup.btest1.model.pojo.MultipartFormAttivita;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;


public class MultipartFormAttivitaUtils {

    public static MultipartFormAttivita decode(MultipartFormDataInput multipartFormDataInput) {
        MultipartFormAttivita multipartFormAttivita = new MultipartFormAttivita();

        try {
            String active = MultipartFormUtils.readTextField(multipartFormDataInput, "active");
            multipartFormAttivita.active = Boolean.valueOf(active);
        } catch (Exception e) {
//         e.printStackTrace();
        }
        try {
            String ore = MultipartFormUtils.readTextField(multipartFormDataInput, "ore");
            multipartFormAttivita.ore = Integer.valueOf(ore);
        } catch (Exception e) {
//         e.printStackTrace();
        }

        try {
            multipartFormAttivita.descrizione = MultipartFormUtils.readTextField(multipartFormDataInput, "descrizione");
        } catch (Exception e) {
//         e.printStackTrace();
        }
        try {
            multipartFormAttivita.codice_progetto = MultipartFormUtils.readTextField(multipartFormDataInput, "codice_progetto");
        } catch (Exception e) {
//         e.printStackTrace();
        }
        try {
            multipartFormAttivita.email_sviluppatore = MultipartFormUtils.readTextField(multipartFormDataInput, "email_sviluppatore");
        } catch (Exception e) {
//         e.printStackTrace();
        }

        try {
            multipartFormAttivita.citta = MultipartFormUtils.readTextField(multipartFormDataInput, "citta");
        } catch (Exception e) {
//         e.printStackTrace();
        }
        try {
            multipartFormAttivita.data = MultipartFormUtils.readTextField(multipartFormDataInput, "data");
        } catch (Exception e) {
//         e.printStackTrace();
        }

        return multipartFormAttivita;
    }

}
