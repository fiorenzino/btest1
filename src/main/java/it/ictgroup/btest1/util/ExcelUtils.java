package it.ictgroup.btest1.util;


import it.ictgroup.btest1.model.Attivita;
import it.ictgroup.btest1.model.Progetto;
import it.ictgroup.btest1.model.Sviluppatore;
import it.ictgroup.btest1.repository.ProgettiRepository;
import it.ictgroup.btest1.repository.SviluppatoriRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.logging.Logger;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by fiorenzo on 02/11/15.
 */
public class ExcelUtils {
    static Logger logger = Logger.getLogger(ExcelUtils.class);

    public static XSSFWorkbook fromAttivita(List<Attivita> attivitas, Map<String, String> progetto, Map<String, String> sviluppatore) {

        String[] columns = {"data", "ore", "codice_progetto","nome_progetto", "cognome", "descrizione", "citta"};
        
        // Create a Workbook
        XSSFWorkbook wb = new XSSFWorkbook();

        /* CreationHelper helps us create instances of various things like DataFormat,
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = wb.getCreationHelper();

        // Create a Sheet
        Sheet sheet = wb.createSheet("rendicontazione");

        // Create a Font for styling header cells
        Font headerFont = wb.createFont();
        ((XSSFFont) headerFont).setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = wb.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Create cells
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Cell Style for formatting Date
        CellStyle dateCellStyle = wb.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

        // Create Other rows and cells with attivitas data
        int rowNum = 1;
        for(Attivita attivita: attivitas) {
            Row row = sheet.createRow(rowNum++);

            Cell data = row.createCell(0);
            data.setCellValue(attivita.data);
            data.setCellStyle(dateCellStyle);

            row.createCell(1)
                    .setCellValue(attivita.ore);

            row.createCell(2)
                    .setCellValue(attivita.codice_progetto);

            row.createCell(3)
                    .setCellValue(progetto.get(attivita.codice_progetto));

            row.createCell(4)
                    .setCellValue(sviluppatore.get(attivita.email_sviluppatore));

            row.createCell(5)
                    .setCellValue(attivita.descrizione);

            row.createCell(6)
                    .setCellValue(attivita.citta);
        }

        // Resize all columns to fit the content size
        for(int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        return wb;
    }
}