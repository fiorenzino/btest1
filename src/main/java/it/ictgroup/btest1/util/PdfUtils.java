package it.ictgroup.btest1.util;

import it.ictgroup.btest1.model.Attivita;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

public class PdfUtils {
    public static ByteArrayOutputStream createPdf(List<Attivita> attivitas, Map<String, String> progetto, Map<String, String> sviluppatore) throws Exception {
        HtlmUtils htlmUtils = new HtlmUtils();

        ITextRenderer renderer = new ITextRenderer();

        // if you have html source in hand, use it to generate document object
        renderer.setDocumentFromString(htlmUtils.getHtml(attivitas, progetto, sviluppatore));
        renderer.layout();

        ByteArrayOutputStream fos = new ByteArrayOutputStream();
        renderer.createPDF(fos);
        fos.close();

        return fos;
    }
}
