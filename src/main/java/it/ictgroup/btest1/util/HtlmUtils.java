package it.ictgroup.btest1.util;

import it.ictgroup.btest1.model.Attivita;
import j2html.tags.DomContent;

import java.util.List;
import java.util.Map;

import static j2html.TagCreator.*;

public class HtlmUtils {
    public String getHtml(List<Attivita> attivitas, Map<String, String> progetto, Map<String, String> sviluppatore) {
        String html = html(body(h1("Resoconto"),
                (DomContent) p(table(tbody(tr(th("Data"), th("Ore"), th("codice_progetto"), th("nome_progetto"), th("cognome"), th("descrizione"), th("citta")),
                        each(attivitas, attivita -> tr(
                                td(String.valueOf(attivita.data)), td(String.valueOf(attivita.ore)), td(String.valueOf(attivita.codice_progetto)), td(String.valueOf(progetto.get(attivita.codice_progetto)))
                                , td(String.valueOf(sviluppatore.get(attivita.email_sviluppatore))), td(String.valueOf(attivita.descrizione)), td(String.valueOf(attivita.citta))
                        ))))
                ))).render();
        return html;
    }


}
