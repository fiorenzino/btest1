package it.ictgroup.btest1.service.rs;

import it.ictgroup.btest1.common.RsRepositoryService;
import it.ictgroup.btest1.management.AppConstants;
import it.ictgroup.btest1.model.Cliente;
import it.ictgroup.btest1.repository.ClientiRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path(AppConstants.CLIENTI_PATH)
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ClientiRepositoryRs extends RsRepositoryService<Cliente> {

    public ClientiRepositoryRs() {
    }

    @Inject
    public ClientiRepositoryRs(ClientiRepository clientiRepository) {
        super(clientiRepository);
    }


    @POST
    public Response persist(Cliente cliente) throws Exception {
        try {
            repository.persist(cliente);
            return jsonResponse(Response.Status.OK, cliente);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GET
    @Path("/{id}")
    public Response fetch(@PathParam("id") String id) {
        try {
            Cliente sviluppatore = repository.find(id);
            return jsonResponse(Response.Status.OK, sviluppatore);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, "msg", e.getMessage());
        }
    }

    @PUT
    @Path("/{id}")
    public Response update(@PathParam("id") String id, Cliente cliente) throws Exception {
        try {
            Cliente old = repository.find(id);
            if (old == null) {
                return jsonResponse(Response.Status.NOT_FOUND, "sviluppatore non trovato per uuid: " + cliente.uuid);
            }
            repository.update(cliente);
            return jsonResponse(Response.Status.OK, cliente);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String uuid) throws Exception {
        try {
            repository.delete(uuid);
            return jsonResponse(Response.Status.NO_CONTENT);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @DELETE
    @Path("/hard/{id}")
    public Response hard_delete(@PathParam("id") String uuid) throws Exception {
        try {
            clientiRepository().hard_delete(uuid);
            return jsonResponse(Response.Status.NO_CONTENT);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GET
    public Response getList(@DefaultValue("0") @QueryParam("startRow") Integer startRow,
                            @DefaultValue("10") @QueryParam("pageSize") Integer pageSize,
                            @QueryParam("orderBy") String orderBy, @Context UriInfo ui)
            throws Exception {
        int listSize = repository.getListSize();
        List<Cliente> list = repository.getList(startRow, pageSize);
        return Response
                .status(Response.Status.OK)
                .entity(list)
                .header("Access-Control-Expose-Headers",
                        "startRow, pageSize, listSize").header("startRow", startRow)
                .header("pageSize", pageSize).header("listSize", listSize).build();
    }

    @GET
    @Path("/search")
    public Response findNome(@QueryParam("nome") String nome) {
        try {
            Cliente sviluppatore = clientiRepository().findByNome(nome);
            return jsonResponse(Response.Status.OK, sviluppatore);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, "msg", e.getMessage());
        }
    }
    private ClientiRepository clientiRepository() {
        return (ClientiRepository) getRepository();
    }
}
