package it.ictgroup.btest1.service.rs;

import it.ictgroup.btest1.common.RsRepositoryService;
import it.ictgroup.btest1.management.AppConstants;
import it.ictgroup.btest1.model.Attivita;
import it.ictgroup.btest1.model.enums.MassiveType;
import it.ictgroup.btest1.model.pojo.MultipartFormAttivita;
import it.ictgroup.btest1.repository.AttivitaRepository;
import it.ictgroup.btest1.repository.AutorizzazioniRepository;
import it.ictgroup.btest1.repository.ProgettiRepository;
import it.ictgroup.btest1.repository.SviluppatoriRepository;
import it.ictgroup.btest1.service.AttivitaService;
import it.ictgroup.btest1.util.AttivitaUtils;
import it.ictgroup.btest1.util.MultipartFormAttivitaUtils;
import it.ictgroup.btest1.util.MultipartFormUtils;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.File;
import java.util.List;

@Path(AppConstants.ATTIVITA_PATH)
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@TransactionTimeout(value = 3600L)
public class AttivitaRepositoryRs extends RsRepositoryService<Attivita> {

    @Inject
    AutorizzazioniRepository autorizzazioniRepository;

    @Inject
    ProgettiRepository progettiRepository;

    @Context
    HttpServletRequest httpServletRequest;

    @Inject
    SviluppatoriRepository sviluppatoriRepository;

    @Inject
    AttivitaService attivitaService;

    public AttivitaRepositoryRs() {
    }

    @Inject
    public AttivitaRepositoryRs(AttivitaRepository attivitaRepository) {
        super(attivitaRepository);
    }


    @POST
    public Response persist(Attivita attivita) throws Exception {
        try {
            AttivitaUtils.verifica(attivita, progettiRepository, attivitaRepository(),
                    sviluppatoriRepository, autorizzazioniRepository);
        } catch (Exception e) {
            return jsonResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
        try {
            repository.persist(attivita);
            return jsonResponse(Response.Status.OK, attivita);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GET
    @Path("/{id}")
    public Response fetch(@PathParam("id") String id) {
        try {
            Attivita attivita = repository.find(id);
            return jsonResponse(Response.Status.OK, attivita);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @PUT
    @Path("/{id}")
    public Response update(@PathParam("id") String id, Attivita attivita) throws Exception {
        try {
            AttivitaUtils.verifica(attivita, progettiRepository, attivitaRepository(),
                    sviluppatoriRepository, autorizzazioniRepository);
        } catch (Exception e) {
            return jsonResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
        try {
            Attivita old = repository.find(id);
            if (old == null) {
                return jsonResponse(Response.Status.NOT_FOUND, "sviluppatore non trovato per uuid: " + attivita.uuid);
            }
            repository.update(attivita);
            return Response.status(Response.Status.OK).entity(attivita).build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String uuid) throws Exception {
        try {
            repository.delete(uuid);
            return jsonResponse(Response.Status.NO_CONTENT);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @DELETE
    @Path("/hard/{id}")
    public Response hard_delete(@PathParam("id") String uuid) throws Exception {
        try {
            attivitaRepository().hard_delete(uuid);
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GET
    public Response getList(@DefaultValue("0") @QueryParam("startRow") Integer startRow,
                            @DefaultValue("10") @QueryParam("pageSize") Integer pageSize,
                            @QueryParam("orderBy") String orderBy, @Context UriInfo ui)
            throws Exception {
        int listSize = repository.getListSize();
        List<Attivita> list = repository.getList(startRow, pageSize);
        return Response
                .status(Response.Status.OK)
                .entity(list)
                .header("Access-Control-Expose-Headers",
                        "startRow, pageSize, listSize").header("startRow", startRow)
                .header("pageSize", pageSize).header("listSize", listSize).build();
    }

    private AttivitaRepository attivitaRepository() {
        return (AttivitaRepository) getRepository();
    }

    @GET
    @Path("/search")
    public Response search(@QueryParam("data_dal") String data_dal,
                           @QueryParam("data_al") String data_al,
                           @QueryParam("email_sviluppatore") String email_sviluppatore,
                           @QueryParam("codice_progetto") String codice_progetto,
                           @QueryParam("codice_cliente") String codice_cliente) {
        try {
            List<Attivita> attivitas = attivitaRepository().report(
                    data_dal,
                    data_al,
                    email_sviluppatore,
                    codice_progetto,
                    codice_cliente);
            return Response.ok(attivitas).build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error reading resource list").build();
        }
    }

}
