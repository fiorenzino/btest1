package it.ictgroup.btest1.service.rs;

import io.undertow.util.DateUtils;
import it.ictgroup.btest1.common.RsRepositoryService;
import it.ictgroup.btest1.management.AppConstants;
import it.ictgroup.btest1.model.Autorizzazione;
import it.ictgroup.btest1.model.Progetto;
import it.ictgroup.btest1.model.Sviluppatore;
import it.ictgroup.btest1.repository.AutorizzazioniRepository;
import it.ictgroup.btest1.repository.ProgettiRepository;
import it.ictgroup.btest1.repository.SviluppatoriRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.DatabaseMetaData;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Path(AppConstants.AUTORIZZAZIONI_PATH)
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AutorizzazioniRepositoryRs extends RsRepositoryService<Autorizzazione> {
    @Inject
    ProgettiRepository progettiRepository;

    @Inject
    SviluppatoriRepository sviluppatoriRepository;

    public AutorizzazioniRepositoryRs() {
    }

    @Inject
    public AutorizzazioniRepositoryRs(AutorizzazioniRepository autorizzazioniRepository) {
        super(autorizzazioniRepository);
    }

    @POST
    public Response persist(Autorizzazione autorizzazione) throws Exception {

        try {
            repository.persist(autorizzazione);
            return jsonResponse(Response.Status.OK, autorizzazione);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }


    @GET
    @Path("/{id}")
    public Response fetch(@PathParam("id") String id) {
        try {
            Autorizzazione autorizzazione = repository.find(id);
            return jsonResponse(Response.Status.OK, autorizzazione);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @PUT
    @Path("/{id}")
    public Response update(@PathParam("id") String id, Autorizzazione autorizzazione) throws Exception {

        try {
            Autorizzazione old = repository.find(id);
            if (old == null) {
                return jsonResponse(Response.Status.NOT_FOUND, "autorizzazione non trovato per uuid: " + autorizzazione.uuid);
            }
            repository.update(autorizzazione);
            return jsonResponse(Response.Status.OK, autorizzazione);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String uuid) throws Exception {
        try {
            repository.delete(uuid);
            return jsonResponse(Response.Status.NO_CONTENT);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GET
    public Response getList(@DefaultValue("0") @QueryParam("startRow") Integer startRow,
                            @DefaultValue("10") @QueryParam("pageSize") Integer pageSize,
                            @QueryParam("orderBy") String orderBy, @Context UriInfo ui)
            throws Exception {
        int listSize = repository.getListSize();
        List<Autorizzazione> list = repository.getList(startRow, pageSize);
        return Response
                .status(Response.Status.OK)
                .entity(list)
                .header("Access-Control-Expose-Headers",
                        "startRow, pageSize, listSize").header("startRow", startRow)
                .header("pageSize", pageSize).header("listSize", listSize).build();
    }

    @GET
    @Path("/CreaAutorizzazioni")
    public void newAutorizzazione() throws ParseException {
        List<Progetto> progetti = progettiRepository.getAllProgetti();
        List<Sviluppatore> sviluppatori = sviluppatoriRepository.getAllSviluppatori();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date_start = format.parse("2018-01-01");
        Date date_end = format.parse("2019-12-31");
        for (Progetto progetto : progetti) {
            for (Sviluppatore sviluppatore : sviluppatori) {
                Autorizzazione autorizzazione = new Autorizzazione();
                autorizzazione.data_fine = date_end;
                autorizzazione.data_inizio = date_start;
                autorizzazione.uuid_progetto = progetto.uuid;
                autorizzazione.uuid_sviluppatore = sviluppatore.uuid;
                autorizzazione.progetto = progetto;
                autorizzazione.sviluppatore = sviluppatore;
                try {
                    persist(autorizzazione);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
