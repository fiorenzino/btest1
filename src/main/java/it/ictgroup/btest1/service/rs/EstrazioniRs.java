package it.ictgroup.btest1.service.rs;

import it.ictgroup.btest1.management.AppConstants;
import it.ictgroup.btest1.model.Attivita;
import it.ictgroup.btest1.model.Sviluppatore;
import it.ictgroup.btest1.model.pojo.Riepilogo;
import it.ictgroup.btest1.model.pojo.RiepilogoSviluppatore;
import it.ictgroup.btest1.repository.AttivitaRepository;
import it.ictgroup.btest1.repository.ProgettiRepository;
import it.ictgroup.btest1.repository.SviluppatoriRepository;
import it.ictgroup.btest1.util.CsvUtils;
import it.ictgroup.btest1.util.ExcelUtils;
import it.ictgroup.btest1.util.HtlmUtils;
import it.ictgroup.btest1.util.PdfUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.logging.Logger;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.*;
import java.util.*;

@Path(AppConstants.ESTRAZIONI_PATH)
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EstrazioniRs {

    @Inject
    AttivitaRepository attivitaRepository;

    Logger logger = Logger.getLogger(getClass());

    @Inject
    ProgettiRepository progettiRepository;

    @Inject
    SviluppatoriRepository sviluppatoriRepository;


    @GET
    @Path("/riepilogo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response riepilogo(@QueryParam("data_dal") String data_dal,
                              @QueryParam("data_al") String data_al,
                              @QueryParam("email_sviluppatore") String email_sviluppatore,
                              @QueryParam("codice_progetto") String codice_progetto,
                              @QueryParam("codice_cliente") String codice_cliente) {
        try {
            Riepilogo riepilogo = new Riepilogo();
            riepilogo.sviluppatori = new ArrayList<>();
            riepilogo.data_al = data_al;
            riepilogo.data_dal = data_dal;
            riepilogo.codice_progetto = codice_progetto;
            riepilogo.codice_cliente = codice_cliente;
            List<String> emails = attivitaRepository.emails(
                    data_dal,
                    data_al,
                    email_sviluppatore,
                    codice_progetto,
                    codice_cliente);
            for (String email : emails) {
                RiepilogoSviluppatore riepilogoSviluppatore = new RiepilogoSviluppatore();
                Sviluppatore sviluppatore = sviluppatoriRepository.findByEmail(email);
                riepilogoSviluppatore.sviluppatore = sviluppatore;
                int ore = attivitaRepository.working_hours(
                        data_dal,
                        data_al,
                        email_sviluppatore,
                        codice_progetto,
                        codice_cliente);
                int giorni = ore / 8;
                if (ore % 8 > 0) {
                    giorni = giorni + 1;
                }
                riepilogoSviluppatore.ore = ore;
                riepilogoSviluppatore.giorni = giorni;
                riepilogo.add(riepilogoSviluppatore);

            }
            return Response.ok(riepilogo).build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error creating riepilogo").build();
        }
    }

    @GET
    @Path("/toCsv")
    @Produces({"text/csv"})
    public Response getCSV(@QueryParam("data_dal") String data_dal,
                           @QueryParam("data_al") String data_al,
                           @QueryParam("email_sviluppatore") String email_sviluppatore,
                           @QueryParam("codice_progetto") String codice_progetto,
                           @QueryParam("codice_cliente") String codice_cliente) {
        try {
            List<Attivita> attivitas = attivitaRepository.report(
                    data_dal,
                    data_al,
                    email_sviluppatore,
                    codice_progetto,
                    codice_cliente);
            List<String> emails = attivitaRepository.emails(
                    data_dal,
                    data_al,
                    email_sviluppatore,
                    codice_progetto,
                    codice_cliente);
            List<String> codici = attivitaRepository.codici(
                    data_dal,
                    data_al,
                    email_sviluppatore,
                    codice_progetto,
                    codice_cliente);
            Map<String, String> sviluppatore = sviluppatoriRepository.getEmailAndSurname(emails);
            Map<String, String> progetto = progettiRepository.getCodeAndName(codici);
            String csv = CsvUtils.fromAttivita(attivitas, progetto, sviluppatore);
            return Response.ok(csv.getBytes(), "text/csv")
                    .header("content-disposition", "attachment; filename=\"report\".csv")
                    .build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error reading resource list").build();
        }
    }

    @GET
    @Path("/toJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJson(@QueryParam("data_dal") String data_dal,
                            @QueryParam("data_al") String data_al,
                            @QueryParam("email_sviluppatore") String email_sviluppatore,
                            @QueryParam("codice_progetto") String codice_progetto,
                            @QueryParam("codice_cliente") String codice_cliente) {
        try {
            List<Attivita> attivitas = attivitaRepository.report(
                    data_dal,
                    data_al,
                    email_sviluppatore,
                    codice_progetto,
                    codice_cliente);
            return Response.status(Response.Status.OK).entity(attivitas).build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error reading resource list").build();
        }
    }


    @GET
    @Path("/toExcel")
    @Produces({"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    public Response getExcel(@QueryParam("data_dal") String data_dal,
                             @QueryParam("data_al") String data_al,
                             @QueryParam("email_sviluppatore") String email_sviluppatore,
                             @QueryParam("codice_progetto") String codice_progetto,
                             @QueryParam("codice_cliente") String codice_cliente) {

        try {
            List<Attivita> attivitas = attivitaRepository.report(
                    data_dal,
                    data_al,
                    email_sviluppatore,
                    codice_progetto,
                    codice_cliente);
            List<String> emails = attivitaRepository.emails(
                    data_dal,
                    data_al,
                    email_sviluppatore,
                    codice_progetto,
                    codice_cliente);
            List<String> codici = attivitaRepository.codici(
                    data_dal,
                    data_al,
                    email_sviluppatore,
                    codice_progetto,
                    codice_cliente);
            Map<String, String> sviluppatore = sviluppatoriRepository.getEmailAndSurname(emails);
            Map<String, String> progetto = progettiRepository.getCodeAndName(codici);
            XSSFWorkbook wb = ExcelUtils.fromAttivita(attivitas, progetto, sviluppatore);
            //Now finally build response and send
            StreamingOutput streamOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream out) throws IOException,
                        WebApplicationException {
                    wb.write(out);
                }
            };
            return Response.ok(streamOutput, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    .header("content-disposition", "attachment; filename=\"report\".xlsx")
                    .build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error reading resource list").build();
        }
    }

    @GET
    @Path("/pdf")
    @Produces("application/pdf")

    public Response getPdf(@QueryParam("data_dal") String data_dal,
                           @QueryParam("data_al") String data_al,
                           @QueryParam("email_sviluppatore") String email_sviluppatore,
                           @QueryParam("codice_progetto") String codice_progetto,
                           @QueryParam("codice_cliente") String codice_cliente) {
        try {
            List<Attivita> attivitas = attivitaRepository.report(
                    data_dal,
                    data_al,
                    email_sviluppatore,
                    codice_progetto,
                    codice_cliente);
            List<String> emails = attivitaRepository.emails(
                    data_dal,
                    data_al,
                    email_sviluppatore,
                    codice_progetto,
                    codice_cliente);
            List<String> codici = attivitaRepository.codici(
                    data_dal,
                    data_al,
                    email_sviluppatore,
                    codice_progetto,
                    codice_cliente);
            Map<String, String> sviluppatore = sviluppatoriRepository.getEmailAndSurname(emails);
            Map<String, String> progetto = progettiRepository.getCodeAndName(codici);
            ByteArrayOutputStream pdf = PdfUtils.createPdf(attivitas, progetto, sviluppatore);

            return Response.ok(pdf.toByteArray()).header("Content-Disposition", "attachment; filename=test.pdf").build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error creating riepilogo").build();
        }
    }
}
