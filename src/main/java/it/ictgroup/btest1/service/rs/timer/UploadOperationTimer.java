package it.ictgroup.btest1.service.rs.timer;

import it.ictgroup.btest1.management.AppConstants;
import it.ictgroup.btest1.model.Attivita;
import it.ictgroup.btest1.model.UploadOperation;
import it.ictgroup.btest1.model.enums.StatusType;
import it.ictgroup.btest1.repository.AttivitaRepository;
import it.ictgroup.btest1.repository.UploadOperationRepository;
import org.jboss.logging.Logger;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


@Singleton
public class UploadOperationTimer {
    Logger logger = Logger.getLogger(getClass());

    @Inject
    AttivitaRepository attivitaRepository;


    @Inject
    UploadOperationRepository uploadOperationRepository;

    @Schedule(hour = "*", minute = "*/30", persistent = false)
    public void exec() {
        try {
            List<UploadOperation> uploadOperations = uploadOperationRepository.getUploadOperationsWithEmptyEndDate();
            if (uploadOperations != null && uploadOperations.size() > 0) {
                logger.info("transmissions size: " + uploadOperations.size());
                AtomicInteger i = new AtomicInteger(0);
                for (UploadOperation uploadOperation : uploadOperations) {
                    List<Attivita> attivitas = attivitaRepository.getAttivitaByUploadOperationUUid(uploadOperation.uuid);
                    if (attivitas.size() == uploadOperation.num_righe) {
                        uploadOperation.statusType = StatusType.COMPLETATO;
                        uploadOperation.data_fine_lavorazione = new Date();
                        uploadOperationRepository.update(uploadOperation);
                        Files.delete(Paths.get(AppConstants.csvUploadPath, uploadOperation.uuid + ".csv"));
                        return;
                    }

                    Date date = new Date();
                    long diff = date.getTime() - uploadOperation.data_creazione.getTime();
                    diff = diff / (60 * 1000);
                    if (diff > 60) {
                        uploadOperation.statusType = StatusType.ERRORE;
                        uploadOperation.data_fine_lavorazione = new Date();
                        uploadOperationRepository.update(uploadOperation);
                    }


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
