package it.ictgroup.btest1.service.rs;

import it.ictgroup.btest1.common.RsRepositoryService;
import it.ictgroup.btest1.management.AppConstants;
import it.ictgroup.btest1.model.Sviluppatore;
import it.ictgroup.btest1.repository.SviluppatoriRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path(AppConstants.SVILUPPATORI_PATH)
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SviluppatoriRepositoryRs extends RsRepositoryService<Sviluppatore> {

    public SviluppatoriRepositoryRs() {
    }

    @Inject
    public SviluppatoriRepositoryRs(SviluppatoriRepository sviluppatoriRepository) {
        super(sviluppatoriRepository);
    }


    @POST
    public Response persist(Sviluppatore sviluppatore) throws Exception {
        try {
            verificaPerEmail(sviluppatore.email);
            repository.persist(sviluppatore);
            return jsonResponse(Response.Status.OK, sviluppatore);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GET
    @Path("/{id}")
    public Response fetch(@PathParam("id") String id) {
        try {
            Sviluppatore sviluppatore = repository.find(id);
            return jsonResponse(Response.Status.OK, sviluppatore);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, "msg", e.getMessage());
        }
    }

    @PUT
    @Path("/{id}")
    public Response update(@PathParam("id") String id, Sviluppatore sviluppatore) throws Exception {
        try {
            Sviluppatore old = repository.find(id);
            if (old == null) {
                return jsonResponse(Response.Status.NOT_FOUND, "sviluppatore non trovato per uuid: " + sviluppatore.uuid);
            }
            verificaPerEmailUpdate(sviluppatore.email, id);

            repository.update(sviluppatore);
            return jsonResponse(Response.Status.OK, sviluppatore);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String uuid) throws Exception {
        try {
            repository.delete(uuid);
            return jsonResponse(Response.Status.NO_CONTENT);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @DELETE
    @Path("/hard/{id}")
    public Response hard_delete(@PathParam("id") String uuid) throws Exception {
        try {
            sviluppatoriRepository().hard_delete(uuid);
            return jsonResponse(Response.Status.NO_CONTENT);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GET
    public Response getList(@DefaultValue("0") @QueryParam("startRow") Integer startRow,
                            @DefaultValue("10") @QueryParam("pageSize") Integer pageSize,
                            @QueryParam("orderBy") String orderBy, @Context UriInfo ui)
            throws Exception {
        int listSize = repository.getListSize();
        List<Sviluppatore> list = repository.getList(startRow, pageSize);
        return Response
                .status(Response.Status.OK)
                .entity(list)
                .header("Access-Control-Expose-Headers",
                        "startRow, pageSize, listSize").header("startRow", startRow)
                .header("pageSize", pageSize).header("listSize", listSize).build();
    }

    @GET
    @Path("/search")
    public Response findEmail(@QueryParam("email") String email) {
        try {
            Sviluppatore sviluppatore = sviluppatoriRepository().findByEmail(email);
            return jsonResponse(Response.Status.OK, sviluppatore);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, "msg", e.getMessage());
        }
    }

    //INTERNAL METHODS

    private void verificaPerEmailUpdate(String email, String uuid) throws Exception {
        boolean esiste = sviluppatoriRepository().verificaPerEmailUpdate(email, uuid);
        if (esiste)
            throw new Exception("Email risulta utilizzato per un'altro Sviluppatore.");
    }

    private void verificaPerEmail(String email) throws Exception {
        boolean esiste = sviluppatoriRepository().verificaPerEmail(email);
        if (esiste)
            throw new Exception("Email risulta utilizzato per un'altro Sviluppatore.");
    }

    private SviluppatoriRepository sviluppatoriRepository() {
        return (SviluppatoriRepository) getRepository();
    }
}
