package it.ictgroup.btest1.service.rs;

import it.ictgroup.btest1.common.RsRepositoryService;
import it.ictgroup.btest1.management.AppConstants;
import it.ictgroup.btest1.model.UploadOperation;
import it.ictgroup.btest1.model.enums.MassiveType;
import it.ictgroup.btest1.model.enums.StatusType;
import it.ictgroup.btest1.model.pojo.MultipartFormAttivita;
import it.ictgroup.btest1.repository.UploadOperationRepository;
import it.ictgroup.btest1.service.AttivitaService;
import it.ictgroup.btest1.util.MultipartFormAttivitaUtils;
import it.ictgroup.btest1.util.MultipartFormUtils;
import org.apache.commons.io.FileUtils;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Path(AppConstants.UPLOAD_OPERATIONS_PATH)
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UploadOperationRepositoryRs extends RsRepositoryService<UploadOperation> {

    @Context
    HttpServletRequest httpServletRequest;

    @Inject
    AttivitaService attivitaService;

    public UploadOperationRepositoryRs() {
    }

    @Inject
    public UploadOperationRepositoryRs(UploadOperationRepository uploadOperationRepository) {
        super(uploadOperationRepository);
    }


    @POST
    public Response persist(UploadOperation uploadOperation) throws Exception {
        try {
            repository.persist(uploadOperation);
            return jsonResponse(Response.Status.OK, uploadOperation);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GET
    @Path("/{id}")
    public Response fetch(@PathParam("id") String id) {
        try {
            UploadOperation uploadOperation = repository.find(id);
            return jsonResponse(Response.Status.OK, uploadOperation);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, "msg", e.getMessage());
        }
    }

    @PUT
    @Path("/{id}")
    public Response update(@PathParam("id") String id, UploadOperation uploadOperation) throws Exception {
        try {
            UploadOperation old = repository.find(id);
            if (old == null) {
                return jsonResponse(Response.Status.NOT_FOUND, "sviluppatore non trovato per uuid: " + uploadOperation.uuid);
            }
            repository.update(uploadOperation);
            return jsonResponse(Response.Status.OK, uploadOperation);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String uuid) throws Exception {
        try {
            repository.delete(uuid);
            return jsonResponse(Response.Status.NO_CONTENT);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }


    @GET
    public Response getList(@DefaultValue("0") @QueryParam("startRow") Integer startRow,
                            @DefaultValue("10") @QueryParam("pageSize") Integer pageSize,
                            @QueryParam("orderBy") String orderBy, @Context UriInfo ui)
            throws Exception {
        int listSize = repository.getListSize();
        List<UploadOperation> list = repository.getList(startRow, pageSize);
        return Response
                .status(Response.Status.OK)
                .entity(list)
                .header("Access-Control-Expose-Headers",
                        "startRow, pageSize, listSize").header("startRow", startRow)
                .header("pageSize", pageSize).header("listSize", listSize).build();
    }


    private void work(MassiveType massiveType, MultipartFormDataInput multipartFormDataInput, UploadOperation uploadOperation) throws Exception {
        File file = null;

        MultipartFormAttivita multipartFormAttivita = MultipartFormAttivitaUtils
                .decode(multipartFormDataInput);
        file = MultipartFormUtils.readFileBytes(multipartFormDataInput, "file", uploadOperation);
        uploadOperation.num_righe = it.ictgroup.btest1.util.FileUtils.countLines(file.getAbsolutePath()) - 1;
        repository.persist(uploadOperation);
        String username = (username() != null) ? "anonymous" : username();
        if (file == null)
            throw new Exception("file error");

        logger.info("files: " + file.getName());
        switch (massiveType) {
            case MASSIVE_SYNC:
                attivitaService.sync(username, file, uploadOperation.uuid);
                break;
            case ASYNC:
                attivitaService.async(username, file, uploadOperation.uuid);
                break;
            case QUEUE:
                attivitaService.queue(username, file, uploadOperation.uuid);
        }
    }

    private String username() {
        if (httpServletRequest.getUserPrincipal() != null && httpServletRequest.getUserPrincipal().getName() != null)
            return httpServletRequest.getUserPrincipal().getName();
        return null;
    }


    @POST
    @Path("/massive")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response massive(MultipartFormDataInput multipartFormDataInput) {
        try {
            UploadOperation uploadOperation = newUploadOperation();
            work(MassiveType.MASSIVE_SYNC, multipartFormDataInput, uploadOperation);
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return jsonResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @POST
    @Path("/queue")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response sync(MultipartFormDataInput multipartFormDataInput) {
        try {
            UploadOperation uploadOperation = newUploadOperation();
            work(MassiveType.QUEUE, multipartFormDataInput, uploadOperation);
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return jsonResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @POST
    @Path("/async")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response async(MultipartFormDataInput multipartFormDataInput) {
        try {
            UploadOperation uploadOperation = newUploadOperation();
            work(MassiveType.ASYNC, multipartFormDataInput, uploadOperation);
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return jsonResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    public UploadOperation newUploadOperation() throws Exception {
        UploadOperation uploadOperation = new UploadOperation();
        uploadOperation.uuid = UUID.randomUUID().toString();
        uploadOperation.data_creazione = new Date();
        uploadOperation.statusType = StatusType.ATTIVO;
        uploadOperation.nome_file = uploadOperation.uuid + ".csv";
        return uploadOperation;
    }

    private UploadOperationRepository uploadOperationRepository() {
        return (UploadOperationRepository) getRepository();
    }
}
