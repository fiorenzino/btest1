package it.ictgroup.btest1.service.rs;

import it.ictgroup.btest1.common.RsRepositoryService;
import it.ictgroup.btest1.management.AppConstants;
import it.ictgroup.btest1.model.Progetto;
import it.ictgroup.btest1.model.Sviluppatore;
import it.ictgroup.btest1.repository.ProgettiRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

@Path(AppConstants.PROGETTI_PATH)
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProgettiRepositoryRs extends RsRepositoryService<Progetto> {

    public ProgettiRepositoryRs() {
    }

    @Inject
    public ProgettiRepositoryRs(ProgettiRepository progettiRepository) {
        super(progettiRepository);
    }

    private void verifica(Progetto progetto) throws Exception {
        if (progettiRepository().findByNomeCodice(progetto.nome, progetto.codice) != 0) {
            throw new Exception("Nome e codice risultano utilizzati per un'altro progetto.");
        }
    }


    @POST
    public Response persist(Progetto progetto) throws Exception {
        try {
            verifica(progetto);
        } catch (Exception e) {
            return jsonResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
        try {
            repository.persist(progetto);
            return jsonResponse(Response.Status.OK, progetto);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }


    @GET
    @Path("/{id}")
    public Response fetch(@PathParam("id") String id) {
        try {
            Progetto progetto = repository.find(id);
            return jsonResponse(Response.Status.OK, progetto);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @PUT
    @Path("/{id}")
    public Response update(@PathParam("id") String id, Progetto progetto) throws Exception {
        try {
            verifica(progetto);
        } catch (Exception e) {
            return jsonResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
        try {
            Progetto old = repository.find(id);
            if (old == null) {
                return jsonResponse(Response.Status.NOT_FOUND, "progetto non trovato per uuid: " + progetto.uuid);
            }
            repository.update(progetto);
            return jsonResponse(Response.Status.OK, progetto);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String uuid) throws Exception {
        try {
            repository.delete(uuid);
            return jsonResponse(Response.Status.NO_CONTENT);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @DELETE
    @Path("/hard/{id}")
    public Response hard_delete(@PathParam("id") String uuid) throws Exception {
        try {
            progettiRepository().delete_hard(uuid);
            return jsonResponse(Response.Status.NO_CONTENT);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GET
    public Response getList(@DefaultValue("0") @QueryParam("startRow") Integer startRow,
                            @DefaultValue("10") @QueryParam("pageSize") Integer pageSize,
                            @QueryParam("orderBy") String orderBy, @Context UriInfo ui)
            throws Exception {
        int listSize = repository.getListSize();
        List<Progetto> list = repository.getList(startRow, pageSize);
        return Response
                .status(Response.Status.OK)
                .entity(list)
                .header("Access-Control-Expose-Headers",
                        "startRow, pageSize, listSize").header("startRow", startRow)
                .header("pageSize", pageSize).header("listSize", listSize).build();
    }


    ProgettiRepository progettiRepository() {
        return ((ProgettiRepository) getRepository());
    }

    @GET
    @Path("/search")
    public Response findCodice(@QueryParam("codice") String codice) {
        try {
            List<Progetto> progetti = progettiRepository().findCodice(codice);
            return jsonResponse(Response.Status.OK, progetti);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return jsonResponse(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }


}
