package it.ictgroup.btest1.service.jms;

import it.ictgroup.btest1.management.AppConstants;
import it.ictgroup.btest1.model.Attivita;
import it.ictgroup.btest1.repository.AttivitaRepository;
import it.ictgroup.btest1.util.AttivitaUtils;
import org.jboss.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

@MessageDriven(name = "EventsMdb", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = AppConstants.ATTIVITA_QUEUE),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "5"),
        @ActivationConfigProperty(propertyName = "transactionTimeout", propertyValue = "3600"),
        @ActivationConfigProperty(propertyName = "dLQMaxResent", propertyValue = "0")})
public class AttivitaMdb implements MessageListener {

    Logger logger = Logger.getLogger(getClass());

    @Inject
    AttivitaRepository attivitaRepository;

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void onMessage(Message message) {
        MapMessage msg = null;
        try {
            msg = (MapMessage) message;
            String line = msg.getString(AppConstants.MSG_LINE);
            String operatore = msg.getString(AppConstants.MSG_OPERATORE);
            String uuid = msg.getString(AppConstants.MSG_UUID);
            Attivita attivita = AttivitaUtils.single(line, AppConstants.SEPARATOR_SYMBOL, uuid);
            attivita.operatore = operatore;
            attivitaRepository.persist(attivita);
        } catch (Throwable t) {
            logger.info("msg error:" + t.getMessage());
        }
    }

}