package it.ictgroup.btest1.service;

import it.ictgroup.btest1.model.Attivita;
import it.ictgroup.btest1.model.UploadOperation;
import it.ictgroup.btest1.model.enums.StatusType;
import it.ictgroup.btest1.model.pojo.MultipartFormAttivita;
import it.ictgroup.btest1.repository.AttivitaRepository;
import it.ictgroup.btest1.repository.AutorizzazioniRepository;
import it.ictgroup.btest1.repository.ProgettiRepository;
import it.ictgroup.btest1.repository.SviluppatoriRepository;
import it.ictgroup.btest1.util.AttivitaUtils;
import it.ictgroup.btest1.util.FileUtils;
import org.jboss.logging.Logger;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.File;
import java.util.List;

@Stateless
@LocalBean
public class AttivitaService {

    Logger logger = Logger.getLogger(getClass());

    @Inject
    ProgettiRepository progettiRepository;

    @Inject
    AttivitaRepository attivitaRepository;

    @Inject
    SviluppatoriRepository sviluppatoriRepository;

    @Inject
    AutorizzazioniRepository autorizzazioniRepository;


    @Inject
    MessageServiceSendToMDB messageServiceSendToMDB;


    @Asynchronous
    public void async(String operatore, File file, String uuid) {
        sync(operatore, file, uuid);
    }


    public void sync(String operatore,  File file, String uuid) {
        List<Attivita> attivitas = attivitas(operatore, file, uuid);
        for (Attivita attivita : attivitas) {
            try {
                AttivitaUtils.verifica(attivita, progettiRepository, attivitaRepository, sviluppatoriRepository, autorizzazioniRepository);
                attivitaRepository.persist(attivita);
            } catch (Exception e) {
                logger.error("error in generating attivitas: " + e.getMessage());
            }
        }
    }

    public void queue(String operatore, File file, String uuid) {
        List<String> lines = FileUtils.readLinesFromTextFile(file.getAbsolutePath(), null);
        for (String line : lines) {
            try {
                messageServiceSendToMDB.line(line, operatore, uuid);
            } catch (Exception e) {
                logger.error("error in generating line: " + e.getMessage());
            }
        }
    }

    private List<Attivita> attivitas(String operatore, File file, String uuid) {
        List<String> lines = FileUtils.readLinesFromTextFile(file.getAbsolutePath(), null);
        List<Attivita> attivitas = null;
        try {
            attivitas = AttivitaUtils
                    .fromListStringToAttivita(operatore, lines, uuid);
        } catch (Exception e) {
            logger.error("error in generating attivitas: " + e.getMessage());
            e.printStackTrace();
        }
        return attivitas;
    }

}
