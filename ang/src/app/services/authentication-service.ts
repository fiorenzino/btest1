import {Observable} from 'rxjs';
import {Injectable} from "@angular/core";
import {Utente} from "../model/utente";
import {APP_NAME} from "../constants/constants";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";

@Injectable()
export class AuthenticationService {

	public utente: Utente;
	private url: string = APP_NAME + 'utenti';

	constructor(private http: HttpClient) {
		this.utente = new Utente();
		this.utente.username = 'pranal';
		this.utente.nome = 'Alessandro Prandini';
		this.utente.ruoli = ['admin'];
	}

	getUtente(): Observable<Utente> {
		if (this.utente == null) {
			return this.http.get<Utente>(this.url)
				.pipe(
					map(res => {
						if (res) {
							this.utente = res;
							return this.utente;
						}
					})
				);
		} else {
			return Observable.of(this.utente);
		}
	}

}
