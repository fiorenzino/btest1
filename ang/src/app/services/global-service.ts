import {Injectable} from "@angular/core";
import * as numeral from "numeral";
import "moment/min/locales";
import {SelectItem} from "primeng/api";
import * as _ from "lodash";

@Injectable()
export class GlobalService {

	public appItems: SelectItem[];
	public tipoDocItems: SelectItem[];
	public statoFatturaItems: SelectItem[];

	constructor() {
		this.initialize();
	}

	private initialize() {
		numeral.register('locale', 'it', {
			delimiters: {
				thousands: '.',
				decimal: ','
			},
			abbreviations: {
				thousand: 'mila',
				million: 'mil',
				billion: 'b',
				trillion: 't'
			},
			ordinal: function (number) {
				return 'º';
			},
			currency: {
				symbol: '€'
			}
		});

		this.appItems = [];
		this.appItems.push({value: 'ACG', label: 'ACG'});
		this.appItems.push({value: 'GESIV', label: 'GESIV'});
		this.appItems.push({value: 'SAP', label: 'SAP'});
		this.appItems.push({value: 'WINWASTE', label: 'WINWASTE'});

		this.tipoDocItems = [];
		this.tipoDocItems.push({value: 'TD01', label: 'Fattura'});
		this.tipoDocItems.push({value: 'TD02', label: 'Anticipo su fattura'});
		this.tipoDocItems.push({value: 'TD03', label: 'Anticipo su parcella'});
		this.tipoDocItems.push({value: 'TD04', label: 'Nota di credito'});
		this.tipoDocItems.push({value: 'TD05', label: 'Nota di debito'});
		this.tipoDocItems.push({value: 'TD06', label: 'Parcella'});

		this.statoFatturaItems = [];
		this.statoFatturaItems.push({value: null, label: '...'});
		this.statoFatturaItems.push({value: 'ELIMINATA', label: 'ELIMINATA'});
		this.statoFatturaItems.push({value: 'CARICATA', label: 'CARICATA'});
		this.statoFatturaItems.push({value: 'GENERATA', label: 'GENERATA'});
		this.statoFatturaItems.push({value: 'ERRORE_CREAZIONE', label: 'ERRORE CREAZIONE'});
		this.statoFatturaItems.push({value: 'VALIDATA', label: 'VALIDATA'});
		this.statoFatturaItems.push({value: 'ERRORE_VALIDAZIONE', label: 'ERRORE VALIDAZIONE'});
		this.statoFatturaItems.push({value: 'RICHIESTA_FIRMA', label: 'RICHIESTA FIRMA'});
		this.statoFatturaItems.push({value: 'ERRORE_RICHIESTA_FIRMA', label: 'ERRORE RICHIESTA FIRMA'});
		this.statoFatturaItems.push({value: 'FIRMATA_IN_ARCHIDOC', label: 'FIRMATA IN ARCHIDOC'});
		this.statoFatturaItems.push({value: 'ERRORE_FIRMA_IN_ARCHIDOC', label: 'ERRORE FIRMA IN ARCHIDOC'});
		this.statoFatturaItems.push({value: 'IN_CONSEGNA', label: 'IN CONSEGNA'});
		this.statoFatturaItems.push({value: 'ERRORE_CONSEGNA', label: 'ERRORE CONSEGNA'});
		this.statoFatturaItems.push({value: 'SCARTATA_FTP_IN_ENERJ', label: 'SCARTATA FTP IN ENERJ'});
		this.statoFatturaItems.push({value: 'CONSEGNATA', label: 'CONSEGNATA'});
		this.statoFatturaItems.push({value: 'CONSEGNATA_IN_SOSTITUTIVA', label: 'CONSEGNATA IN SOSTITUTIVA'});
		this.statoFatturaItems.push({value: 'PREPARATA', label: 'PREPARATA'});
		this.statoFatturaItems.push({value: 'FIRMATA', label: 'FIRMATA'});
		this.statoFatturaItems.push({value: 'DA_INVIARE', label: 'DA INVIARE'});
		this.statoFatturaItems.push({value: 'IN_TRASMISSIONE', label: 'IN TRASMISSIONE'});
		this.statoFatturaItems.push({value: 'TRASMESSA', label: 'TRASMESSA'});
		this.statoFatturaItems.push({value: 'NON_RECAPITATA', label: 'NON RECAPITATA'});
		this.statoFatturaItems.push({value: 'ACCETTATA', label: 'ACCETTATA'});
		this.statoFatturaItems.push({value: 'RIFIUTATA', label: 'RIFIUTATA'});
		this.statoFatturaItems.push({value: 'DA_CONSERVARE', label: 'DA CONSERVARE'});
		this.statoFatturaItems.push({value: 'DECORRENZA_TERMINI', label: 'DECORRENZA TERMINI'});
		this.statoFatturaItems.push({value: 'NON_ASSOCIATA_A_LOTTO', label: 'NON ASSOCIATA A LOTTO'});
		this.statoFatturaItems.push({value: 'IN_CONSERVAZIONE', label: 'IN CONSERVAZIONE'});

		this.statoFatturaItems.push({value: 'ERRORE_XMLSCHEMA', label: 'ERRORE XMLSCHEMA'});
		this.statoFatturaItems.push({value: 'OK_SCHEMATRON', label: 'OK SCHEMATRON'});
	}


	public decodeStatoFattura(code: string): string {
		if (code) {
			const item: SelectItem = _.find(this.statoFatturaItems, ['value', code])
			if (item) {
				return item.label;
			}
		}
		return null;
	}
}
