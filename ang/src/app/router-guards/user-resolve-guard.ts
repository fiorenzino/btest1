import {Resolve, Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Rx";
import {AuthenticationService} from "../services/authentication-service";

@Injectable()
export class UserResolveGuard implements Resolve<boolean> {
	constructor(private authenticationService: AuthenticationService,
				private router: Router) {
	}

	resolve(): Observable<boolean> | boolean {
		return this.authenticationService.getUtente()
			.map(
				element => {
					return true;
				})
			.catch(
				error => {
					this.handleError(error);
					return Observable.of(false);
				});
	}

	private handleError(err: any) {
		this.router.navigate(['/logout']);
		return Observable.of(false);
	}
}
