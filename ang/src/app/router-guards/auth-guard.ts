import {CanActivate} from "@angular/router";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Rx";
import {AuthenticationService} from "../services/authentication-service";

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private authenticationService: AuthenticationService) {
    }

    canActivate(): Observable<boolean> {
        return this.authenticationService.getUtente()
            .map(user => {
                if (user != null)
                    return true;
            }).take(1);
    }
}
