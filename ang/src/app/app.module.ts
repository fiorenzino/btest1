import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {routing} from './app.routes';
import {OutputNumberPipe} from './pipes/output-number.pipe';
import {MySharedModule} from './modules/myshared.module';
import {OutputDatePipe} from './pipes/output-date.pipe';
import {OutputDateTimePipe} from './pipes/output-date-time.pipe';
import {AppTopBar} from './app.topbar.component';
import {AppFooter} from './app.footer.component';
import {InlineProfileComponent} from './app.profile.component';
import {AppMenuComponent, AppSubMenu} from './app.menu.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {LogoutComponent} from "./components/logout/logout.component";
import {HomePageComponent} from "./components/homepage/homepage.component";
import {BasicHttpInterceptor} from "./services/http-interceptors/basic-http-interceptor";
import {UserResolveGuard} from "./router-guards/user-resolve-guard";
import {LoaderComponent} from "./components/loader/loader.component";
import {
	ConfirmDialogModule,
	DropdownModule,
	InputTextareaModule,
	InputTextModule,
	MessagesModule
} from "primeng/primeng";
import {SharedModule} from "primeng/shared";
import {TableModule} from "primeng/table";
import {DialogModule} from "primeng/dialog";
import {GrowlModule} from "primeng/growl";
import {ConfirmationService} from "primeng/api";
import {GlobalService} from "./services/global-service";
import {UpperCaseInputDirective} from './directives/upper-case-input.directive';
import {NumberInput} from "./shared/number-input";
import {IntegerInput} from "./shared/integer-input";

@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		HttpClientModule,
		routing,
		MySharedModule.forRoot(),

		MessagesModule,
		ConfirmDialogModule,
		DropdownModule,
		InputTextModule,
		InputTextareaModule,
		SharedModule,
		TableModule,
		DialogModule,
		GrowlModule,
	],
	declarations: [
		AppComponent,
		AppTopBar,
		AppFooter,
		InlineProfileComponent,
		AppMenuComponent,
		AppSubMenu,

		NumberInput,
		IntegerInput,

		OutputDatePipe,
		OutputDateTimePipe,
		OutputNumberPipe,
		LoaderComponent,

		LogoutComponent,
		HomePageComponent,
		UpperCaseInputDirective,

	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: BasicHttpInterceptor,
			multi: true
		},
		GlobalService,
		ConfirmationService,
		UserResolveGuard
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
