import {RouterModule, Routes} from "@angular/router";
import {LogoutComponent} from "./components/logout/logout.component";
import {UserResolveGuard} from "./router-guards/user-resolve-guard";
import {HomePageComponent} from "./components/homepage/homepage.component";

export const MainRoutes: Routes = [
	{path: '', redirectTo: 'home', pathMatch: 'full'},
	{
		path: 'home',
		resolve: {user: UserResolveGuard},
		component: HomePageComponent
	},
	{
		path: 'logout',
		component: LogoutComponent
	}
];

export const routing = RouterModule.forRoot(MainRoutes, {useHash: true});
