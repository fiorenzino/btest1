import {Component, OnInit} from "@angular/core";
import {Utente} from "../../model/utente";
import {AuthenticationService} from "../../services/authentication-service";
import {Router} from "@angular/router";

@Component({
	templateUrl: './homepage.component.html',
	styleUrls: ['./homepage.component.css']
})
export class HomePageComponent implements OnInit {

	public utente: Utente;
	public combined: any = {};

	constructor(private authenticationService: AuthenticationService,
				private router: Router) {
		this.utente = new Utente();
	}

	ngOnInit() {
		this.authenticationService.getUtente().subscribe(
			utente => {
				if (utente == null || utente.ruoli == null || utente.ruoli.length == 0) {
					console.log("should not happen. we have CAS!");
				}
				this.utente = utente;
			}
		);
	}

	public get nomeUtente(): string {
		if (this.utente) {
			return this.utente.nome;
		}
		return null;
	}
}
