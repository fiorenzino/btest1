export var PERMISSION = {
	'ADMIN': ['Admin'],
	'VIEW': ['Admin', 'Web']
};

export const APP_PROTOCOL = window.location.protocol + '//';
export const APP_HOST = window.location.hostname + ':';
export const APP_PORT = window.location.port;
export const APP_CONTEXT = '/fatturepa';
export const APP_API = '/api/v1/';
export const APP_NAME = APP_PROTOCOL + APP_HOST + APP_PORT + APP_CONTEXT + APP_API;

export const APP_VERSION = '1.0';

export const CALENDAR_IT = {
    clear: 'Cancella',
    today: 'Oggi',
    monthNames: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno',
        'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
    monthNamesShort: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu',
        'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
    dayNames: ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDayOfWeek: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};