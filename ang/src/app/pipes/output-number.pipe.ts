import {PipeTransform, Pipe} from "@angular/core";
import * as numeral from "numeral";

@Pipe({
	name: 'outputnumber'
})
export class OutputNumberPipe implements PipeTransform {
	constructor() {
	}

	transform(value, maxdecimals?: number, mindecimals?: number, nullAsZero?: boolean, grouping?: boolean) {
		if (value == undefined || value == null || value == '') {
			if (nullAsZero) {
				return '0';
			}
			return null;
		} else {
			numeral.locale('en');
			let num = numeral(value);
			numeral.locale('it');

			let format: string;
			if (grouping) {
				format = '0,0';
			} else {
				format = '0';
			}
			const _maxdecimals = Math.floor((maxdecimals && maxdecimals >= 1 ? maxdecimals : 0));
			let _mindecimals = Math.floor((mindecimals && mindecimals <= maxdecimals ? mindecimals : 0));

			if (_maxdecimals > 0) {
				format = format + '.';
				for (let i = 0; i < _maxdecimals; i++) {
					if (_mindecimals == 0) {
						format = format + '[';
					}
					format = format + '0';
					_mindecimals--;
				}
				if (_mindecimals < 0) {
					format = format + ']';
				}
			}
			console.log('format:', format);
			return num.format(format);
		}
	}
}
