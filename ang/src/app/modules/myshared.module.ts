import {NgModule, ModuleWithProviders} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {RouterModule} from "@angular/router";
import {AuthGuard} from "../router-guards/auth-guard";
import {AuthenticationService} from "../services/authentication-service";
import {Permit} from "../directives/permit";

@NgModule({
    imports: [CommonModule, HttpModule, RouterModule],
    declarations: [Permit],
    exports: [CommonModule, FormsModule, ReactiveFormsModule, Permit]
})
export class MySharedModule {

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: MySharedModule,
            providers: [AuthenticationService, AuthGuard]
        };
    }
}

@NgModule({
    exports: [MySharedModule]
})
export class MySharedRootModule {
}
