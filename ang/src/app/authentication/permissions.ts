export class Permissions {

    public static get acls(): Map<string, string[]> {
        return new Map<string, string[]>()
            .set('ADMIN', ['Admin'])
            .set('VIEW', ['Admin', 'WebCliente', 'WebAzienda'])
            .set('NOT_ADMIN', ['WebCliente', 'WebAzienda']);
    }

}